import * as React from "react";
import L from "leaflet";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";

import iconImage from "../images/pin-blue.png";
import iconRetinaImage from "../images/pin-blue-2x.png";
import iconRedImage from "../images/pin-red.png";
import iconRedRetinaImage from "../images/pin-red-2x.png";
import iconGreenImage from "../images/pin-green.png";
import iconGreenRetinaImage from "../images/pin-green-2x.png";
import markerShadow from "../images/marker-shadow.png";

import TrafficDensity from "./TrafficDensity";

let icon = L.icon({
    iconUrl: iconImage,
    iconRetinaUrl: iconRetinaImage,
    shadowUrl: markerShadow,
    iconSize: [35, 35],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41],
});
let iconRed = L.icon({
    iconUrl: iconRedImage,
    iconRetinaUrl: iconRedRetinaImage,
    shadowUrl: markerShadow,
    iconSize: [35, 35],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41],
});
let iconGreen = L.icon({
    iconUrl: iconGreenImage,
    iconRetinaUrl: iconGreenRetinaImage,
    shadowUrl: markerShadow,
    iconSize: [35, 35],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41],
});

export interface IBikeStationsProps {
    bikesData: any[];
    bikesPredictionData: any[];
    projectedBikes: number;
}

export default class BikeStations extends React.Component<IBikeStationsProps> {
    render() {
        return (
            <div id="map" data-testid="bike-stations">
                <MapContainer
                    center={[53.3548, -6.2603]}
                    zoom={15}
                    scrollWheelZoom={true}
                >
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />

                    {
                    this.props.bikesData.length &&
                        this.props.bikesData.map((bikeStation, key) => (
                            <Marker
                                key={bikeStation["id"]}
                                position={[
                                    Number(bikeStation["lat"]),
                                    Number(bikeStation["longi"]),
                                ]}
                                icon={
                                    25 >=
                                    (100 * Number(bikeStation["avl_bks"])) /
                                        Number(bikeStation["bks_std"])
                                        ? iconRed
                                        : 75 <
                                          (100 *
                                              Number(bikeStation["avl_bks"])) /
                                              Number(bikeStation["bks_std"])
                                        ? iconGreen
                                        : icon
                                }
                            >
                                <Popup
                                    position={[
                                        Number(bikeStation["lat"]),
                                        Number(bikeStation["longi"]),
                                    ]}
                                >
                                    <div>
                                        <h2>
                                            {"Name: " + bikeStation["addr_ln"]}
                                        </h2>
                                        <h2>
                                            {"Status: " + bikeStation["sta"]}
                                        </h2>
                                        <h2>
                                            {"Available Bikes: " +
                                                bikeStation["avl_bks"]}
                                        </h2>
                                        <h2>
                                            {"Bike Stand Capacity: " +
                                                bikeStation["bks_std"]}
                                        </h2>
                                        <h2>
                                            {"Available Bikes after 1 Hr: " +
                                                this.props.bikesPredictionData[key]["predicted_value"].toFixed(0)}
                                        </h2>
                                    </div>
                                </Popup>
                            </Marker>
                        ))
                    }
                </MapContainer>
            </div>
        );
    }
}
