import { render, screen, within } from "@testing-library/react";
import { MemoryRouter, Route, Routes } from "react-router-dom";
import Dashboard from "../views/Dashboard";
import { AuthContext } from "../Auth";

type AuthContextType = {
  auth: boolean,
  setAuth: Function,
  user: boolean,
  fetchUserDetails: () => Promise<any>,
  login: (email: string, password: string) => Promise<any>,
  signup: (name: string, email: string, password: string, role: string) => Promise<any>,
  logout: () => Promise<void>,
}

const renderDashboard = (auth: boolean) => {
  const authContextValue: AuthContextType = {
    auth,
    setAuth: () => {},
    user: false,
    fetchUserDetails: async () => {},
    login: async (email: string, password: string) => {},
    signup: async (name: string, email: string, password: string, role: string) => {},
    logout: async () => {},
  };

  render(
    <MemoryRouter initialEntries={["/dashboard"]}>
      <AuthContext.Provider value={authContextValue}>
        <Routes>
          <Route path="/dashboard/*" element={<Dashboard />} />
        </Routes>
      </AuthContext.Provider>
    </MemoryRouter>
  );
};

describe("Dashboard", () => {
  test("renders 'You need to sign in' when not authenticated", () => {
    renderDashboard(false);
    expect(screen.getByText("You need to sign in")).toBeInTheDocument();
  });

  test("renders dashboard menu when authenticated", () => {
    renderDashboard(true);

    const menuItems = [
      "Bike Stations",
      "Bus Routes",
      "Reroutes",
      "Dart Stations",
      "Luas Stations",
      "Footfall",
    ];

    menuItems.forEach((item) => {
      expect(screen.getByText(item)).toBeInTheDocument();
    });
  });

  test("navigates to /dashboard/analytics by default when authenticated", () => {
    renderDashboard(true);

    // Replace this with the actual component you expect to render at /dashboard/analytics
    expect(screen.getByText("Luas Stations")).toBeInTheDocument();
  });
});