import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import AuthProvider from "./Auth";
import Root from "./routes/root";
import Login from "./views/Login";
import Dashboard from "./views/Dashboard";
import Bikes from "./views/Bikes";
import Buses from "./views/Buses";
import Darts from "./views/Darts";
import "./App.css";

// Add routing.
import {
    createBrowserRouter,
    Navigate,
    RouterProvider,
} from "react-router-dom";

import reportWebVitals from "./reportWebVitals";
import Footfall from "./views/Footfall";
import Luas from "./views/Luas";
import Analytics from "./views/Analytics";
import Reroutes from "./views/Reroutes";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Root />,
        errorElement: "",
        children: [
            {
                path: "login",
                element: <Login />,
            },
            {
                path: "dashboard",
                element: <Dashboard />,
                children: [
                    {
                        path: "analytics",
                        element: <Analytics />,
                    },
                    {
                        path: "bike-stations",
                        element: <Bikes />,
                    },
                    {
                        path: "bus-routes",
                        element: <Buses />,
                    },
                    {
                        path: "bus-reroutes",
                        element: <Reroutes />,
                    },
                    {
                        path: "dart-stations",
                        element: <Darts />,
                    },
                    {
                        path: "luas-stations",
                        element: <Luas />,
                    },
                    {
                        path: "footfall",
                        element: <Footfall />,
                    }
                ]
            },
        ],
    },
    {
        path: "*",
        element: <Navigate to="/login" replace />,
    },
]);

const root = ReactDOM.createRoot(
    document.getElementById("root") as HTMLElement
);

root.render(
    <React.StrictMode>
        <AuthProvider>
            <RouterProvider router={router} />
        </AuthProvider>
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
if ( "development" === process.env.NODE_ENV ) {
    reportWebVitals(console.log);
}