import { createContext, ReactNode, useContext, useEffect, useState } from 'react';
import axios from 'axios';

type AuthContextType = {
  auth: boolean,
  setAuth: Function,
  user: boolean,
  fetchUserDetails: () => Promise<any>,
  login: (email: string, password: string) => Promise<any>,
  signup: (name: string, email: string, password: string, role: string) => Promise<any>,
  logout: () => Promise<void>,
}

export const AuthContext = createContext<AuthContextType>({
  auth: false,
  setAuth: () => {},
  user: false,
  fetchUserDetails: async () => {},
  login: async (email: string, password: string) => {},
  signup: async (name: string, email: string, password: string, role: string) => {},
  logout: async () => {},
});

export function useAuth() {
  return useContext(AuthContext);
}

export function useUserApi() {
  const { setAuth } = useAuth();

  async function fetchUserDetails() {
    try {
      const response = await axios.get('https://flask-python-webapp-quickstart-ase.azurewebsites.net/user');
      return response.data;
    } catch (error) {
      // console.error('Error fetching user details:', error);
      // throw error;
    }
  }

  async function login(email: string, password: string) {
    try {
      // const response = await axios.post('https://flask-python-webapp-quickstart-ase.azurewebsites.net/login', { email, password });
      setAuth(true);
      console.log('login')
      // console.log(response.data)
      localStorage.setItem('scdAuth', 'true');
      // return response.data;
    } catch (error) {
      // console.error('Error logging in:', error);
      // throw error;
    }
  }

  async function signup(name: string, email: string, password: string, role: string) {
    try {
      const response = await axios.post('https://flask-python-webapp-quickstart-ase.azurewebsites.net/signup', { name, email, password, role });
      return response.data;
    } catch (error) {
      // console.error('Error signing up:', error);
      // throw error;
    }
  }

  async function logout() {
    try {
      console.log('logout')
      // await axios.get('https://flask-python-webapp-quickstart-ase.azurewebsites.net/logout');
      setAuth(false);
      localStorage.removeItem('scdAuth');
    } catch (error) {
      // console.error('Error logging out:', error);
      // throw error;
    }
  }

  return { fetchUserDetails, login, signup, logout };
}

interface Props {
  children?: ReactNode
  // any props that come into the component
}

const AuthProvider = ({ children, ...props }: Props) => {
  const [auth, setAuth] = useState(false);
  const [user, setUser] = useState(false);
  const { fetchUserDetails, login, signup, logout } = useUserApi();

  useEffect(() => {
    const isAuth = () => {
      const auth = localStorage.getItem('scdAuth')
      setUser( auth ? true : false ) ;
    };

    isAuth();
  }, [auth]);

  return (
    <AuthContext.Provider value={{ auth, setAuth, user, fetchUserDetails, login, signup, logout }}>
      {children}
    </AuthContext.Provider>
  );
};


export default AuthProvider;