import React, { useEffect } from "react";
// import { LatLngExpression } from "leaflet";
import { LayerGroup, CircleMarker, Popup } from "react-leaflet";
import axios from "axios";

const fillRedOptions = { fillColor: "red" }

export default function TrafficDensity() {
    const [trafficDensityData, setTrafficData] = React.useState([])

    useEffect(() => {
        async function fetchTrafficData(apiUrl: string) {
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(
                        `Traffic Density API Request Status: ${response.status}`
                    );
                    if (200 === response.status) {
                        console.log(response.data)
                        setTrafficData(response.data)
                    }
                })
                .catch(function (error) {
                    console.log(error.toJSON())
                });
        }
        fetchTrafficData( process.env.REACT_APP_TRAFFIC_DATA_API as string )
    }, []);

    return (
        <LayerGroup>
            {trafficDensityData.length &&
                trafficDensityData.map((data) => {
                    return (
                        <CircleMarker
                            key={data["id"]}
                            center={[data["lat"], data["lon"]]}
                            pathOptions={fillRedOptions}
                            radius={10}
                        >
                            <Popup className="cctvPopup"><img src={data["url"]} alt={data["id"]} /></Popup>
                        </CircleMarker>
                    );
                })}
        </LayerGroup>
    );
}
