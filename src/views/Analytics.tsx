import React, { useEffect, useRef } from "react";
import axios from "axios";
import geojson from "../data/shapes_gdf.json";
import { Line } from 'react-chartjs-2';
import 'chart.js/auto'; // for charts to display
import bikesHistoryAll from "../data/history_bikes_all.json";
import pedistrianDataAll from "../data/history_pedistrians.json";
import Select from 'react-select'

console.log('bikes histroy')
console.log(typeof(Object.values(bikesHistoryAll)))
console.log(bikesHistoryAll)

type Geo = {
    type: string;
    features: any;
};

const geoJson = geojson as Geo;

type iBikeStationProps = {
    id: number;
    hrvst_ts: number;
    stn_id: number;
    avl_bks_std: number;
    bks_std: number;
    avl_bks: number;
    bnk: string;
    bns: string;
    ls_updt_ts: number;
    sta:string;
    addr_ln: string;
    stn_nm: string;
    lat: string;
    longi: string
}

type iBikesHistoryDatasets = {
    label: string;
    data: any[];
    fill: boolean;
    backgroundColor: string;
    borderColor: string;
}

type iBikesHistory = {
    labels: any[];
    datasets: iBikesHistoryDatasets[];
}

const initialBikesHistoryData: iBikesHistory = {
    labels: [],
    datasets: [],
};

export default function Analytics() {

    const ref = useRef();

    const [ bikesData, setBikesData ] = React.useState([])
    const [ redBikesStations, setRedBikesStations ] = React.useState<iBikeStationProps[]>([])
    const [ greenBikesStations, setGreenBikesStations ] = React.useState<iBikeStationProps[]>([])
    const [ trafficDensityData, setTrafficData ] = React.useState([])
    const [ busRoutes, setBusRoutes ] = React.useState(0)
    const [ dartsData, setDartsData ] = React.useState(0)
    const [ dartLiveData, setDartLiveData ] = React.useState(0)
    const [ luasData, setLuasData ] = React.useState(0)
    const [ bikesHistoryData, setBikesHistoryData ] = React.useState<iBikesHistory>(initialBikesHistoryData)

    useEffect(() => {
        async function fetchBikesData(apiUrl: string) {
            await axios
                .get(apiUrl)
                .then(async (response) => {
                    console.log(`Bikes API Request Status: ${response.status}`);
                    if (200 === response.status) {
                        setBikesData( response.data );
                        const redBikes = await response.data.length && response.data.filter( (bikeStation: iBikeStationProps) => {
                            if ( 25 >= (100 * Number(bikeStation.avl_bks)) / Number(bikeStation.bks_std)) {
                                return bikeStation
                            }
                            return false
                        })
                        setRedBikesStations( redBikes )
                        const greenBikes = await response.data.length && response.data.filter( (bikeStation: iBikeStationProps) => {
                            if ( 75 < (100 * Number(bikeStation.avl_bks)) / Number(bikeStation.bks_std)) {
                                return bikeStation
                            }
                            return false
                        })
                        setGreenBikesStations( greenBikes )
                    }
                })
                .catch(function (error) {
                    console.log(error.toJSON());
                });
        };
        fetchBikesData( process.env.REACT_APP_BIKES_DATA_API as string )
        async function fetchTrafficData(apiUrl: string) {
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(
                        `Traffic Density API Request Status: ${response.status}`
                    );
                    if (200 === response.status) {
                        console.log(response.data)
                        setTrafficData(response.data)
                    }
                })
                .catch(function (error) {
                    console.log(error.toJSON())
                });
        }
        fetchTrafficData( process.env.REACT_APP_TRAFFIC_DATA_API as string )
        // Bus Routes Count.
        function busRouteOptions() {
            const unique = Array.from(
                new Set(
                    geoJson.features.map((item: any) => {
                        if (item.properties.route_short_name) {
                            return item.properties.route_short_name;
                        } else if (item.properties.routes) {
                            return item.properties.routes[0].route_short_name;
                        } else {
                            return null;
                        }
                    })
                )
            );
            const options = unique.map((route: any) => {
                return { value: route, label: route };
            });
            return options.length;
        }
        setBusRoutes(busRouteOptions)
        async function fetchDartsData(apiUrl: string) {
            console.log('sending dart request...')
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(`Darts Data Request Status: ${response.status}`);
                    console.log(response.data)
                    if (200 === response.status) {
                        setDartsData( response.data.length );
                    }
                })
                .catch(function (error) {
                    console.log(error.toJSON());
                });
        };
        fetchDartsData( process.env.REACT_APP_DARTS_DATA_API as string )
        async function fetchDartLiveData(apiUrl: string) {
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(`Dart Live Stations Request Status: ${response.status}`);
                    console.log(response.data)
                    if (200 === response.status) {
                        setDartLiveData( response.data.length );
                    }
                })
                .catch(function (error) {
                    console.log(error.toJSON());
                });
        };
        fetchDartLiveData( process.env.REACT_APP_DART_LIVE_DATA_API as string )
        async function fetchLuasData(apiUrl: string) {
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(`Darts Data Request Status: ${response.status}`);
                    if (200 === response.status) {
                        console.log(response.data)
                        setLuasData( response.data.length );
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        };
        fetchLuasData( process.env.REACT_APP_LUAS_STATIONS_LIST_DATA_API as string )
    }, []);

    const getBikesHistoryDates = () => {
        const bikesHistoryValues = Object.values(bikesHistoryAll);
        const bikesHistoryDates = bikesHistoryValues.map((item) => {
                return {
                    id: item.cycl_time_id,
                    date: item.hrvst_ts
                }
            })
            .filter((value, index, self) => {
                return (
                  index ===
                  self.findIndex((v) => v.id === value.id)
                );
            });
        const options = bikesHistoryDates.map((item: any) => {
            return { value: item.id, label: item.date };
        });
        return options.reverse();
    }

    const getBikesData = (id: number) => {
        const bikesHistoryValues = Object.values(bikesHistoryAll);
        const bikesData = bikesHistoryValues.filter(
            (item) => item.cycl_time_id === id
        );
        return bikesData
    }

    const handleBikesHistoryChange = (e: any) => {
        const bikesData = getBikesData(e.value)
        const bikesHistoryData = {
            labels: bikesData.map((item) => item.addr_ln),
            datasets: [
              {
                label: 'Available Bikes',
                data: bikesData.map((item) => {
                    return item.avl_bks_std
                }),
                fill: true,
                backgroundColor: 'rgba(0,199,144,0.2)',
                borderColor: 'rgba(0,199,144,1)'
              },
              {
                label: 'Total Bikes',
                data: bikesData.map((item) => {
                    return item.bks_std
                }),
                fill: true,
                backgroundColor: 'rgba(245,208,91,0.2)',
                borderColor: 'rgba(245,208,91,1)'
              }
            ],
        };
        setBikesHistoryData(bikesHistoryData)
    }

    return (
        <>
            <div className="grid grid-cols-3 gap-4">
                <div className="rounded-md shadow-md p-4">
                    <div className="text-6xl font-medium">{bikesData.length}</div>
                    <div className="mt-5 mb-2">Bike Stations</div>
                    <button className="rounded-full bg-rose-500 text-white text-sm px-2 mr-2">Low: {redBikesStations.length}</button>
                    <button className="rounded-full bg-green-500 text-white text-sm px-2">High: {greenBikesStations.length}</button>
                </div>
                <div className="rounded-md shadow-md p-4">
                    <div className="text-6xl font-medium">122</div>
                    <div className="mt-5 mb-2">Street Cameras</div>
                    <button className="rounded-full bg-rose-500 text-white text-sm px-2 mr-2">Dense: {trafficDensityData.length}</button>
                    <button className="rounded-full bg-green-500 text-white text-sm px-2 mr-2">Sparse: {(122 - trafficDensityData.length)}</button>
                </div>
                <div className="rounded-md shadow-md p-4">
                    <div className="text-6xl font-medium">{busRoutes}</div>
                    <div className="mt-5 mb-2">Bus Routes</div>
                </div>
                <div className="rounded-md shadow-md p-4">
                    <div className="text-6xl font-medium">{dartsData}</div>
                    <div className="mt-5 mb-2">Dart Stations</div>
                </div>
                <div className="rounded-md shadow-md p-4">
                    <div className="text-6xl font-medium">{dartLiveData}</div>
                    <div className="mt-5 mb-2">Trains Running - Live</div>
                </div>
                <div className="rounded-md shadow-md p-4">
                    <div className="text-6xl font-medium">{luasData}</div>
                    <div className="mt-5 mb-2">Luas Stations</div>
                </div>
            </div>
            <div className="mt-5">
                <div className="rounded-md shadow-md p-4">
                    <h2 className="mb-5">Historical Data: Bikes</h2>
                    <Select className="react-select-container" options={getBikesHistoryDates()} placeholder="Select a time" onChange={handleBikesHistoryChange} menuPortalTarget={document.body} styles={{
                        control: base => ({ ...base, marginBottom: 20 }),
                        menuPortal: base => ({ ...base, zIndex: 9999 }),
                        input: base => ({ ...base, boxShadow: 'none' }),
                    }} />
                    {
                        bikesHistoryData.labels.length > 0 && <Line
                            ref={ref}
                            data={bikesHistoryData}
                            />
                    }
                </div>
            </div>
            {/* <div className="mt-5">
                <div className="rounded-md shadow-md p-4">
                    {<Line
                        ref={ref}
                        data={pedistrianDataChart.data}
                        options={pedistrianDataChart.options}
                    />}
                </div>
            </div> */}
        </>
    );
}
