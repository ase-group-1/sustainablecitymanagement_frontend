import * as React from "react";
import { MapContainer, TileLayer, GeoJSON, Polygon, Marker, Popup } from "react-leaflet";
import { GeoJsonObject } from 'geojson';
import TrafficDensity from "./TrafficDensity";
import { LatLngExpression } from "leaflet";
import L from "leaflet";

import iconImage from "../images/pin-blue.png";
import iconRetinaImage from "../images/pin-blue-2x.png";
import markerShadow from "../images/marker-shadow.png";

// interface LatLng {
// 	0: number;
// 	1: number;
// }

// interface Polygon {
//   [index: number]: LatLng;
// }

let icon = L.icon({
    iconUrl: iconImage,
    iconRetinaUrl: iconRetinaImage,
    shadowUrl: markerShadow,
    iconSize: [35, 35],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41],
});

export interface IBusRoutesProps {
	defaultCenter: {
		lat: number,
		lng: number
	},
	defaultZoom: number,
	busRoute: string,
	routes: {
		type: string,
		features: any[]
	},
	polygons: LatLngExpression[][],
	shapes: any,
	stops: any,
	route: string,
	activeTripId: string
}

export default class BusRoutes extends React.Component<IBusRoutesProps> {
  	render() {
		return (
			<MapContainer key={this.props.busRoute} center={this.props.defaultCenter} zoom={this.props.defaultZoom}>
				<TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" attribution="&copy; <a href=&quot;https:www.openstreetmap.org/copyright&quot;>OpenStreetMap</a> contributors" />
				<TrafficDensity />
				<GeoJSON key={this.props.busRoute} data={this.props.routes as GeoJsonObject} />
				<Polygon pathOptions={{ color: 'purple' }} positions={this.props.polygons} />
				{'' !== this.props.activeTripId && <GeoJSON key={this.props.route+'shapes'} data={this.props.shapes as GeoJsonObject} /> }
				{
                    '' !== this.props.activeTripId && this.props.stops && this.props.stops.features && this.props.stops.features.length &&
						this.props.stops && this.props.stops.features && this.props.stops.features.map((feature: any) => (
                            <Marker
                                key={feature.id}
                                position={[
                                    Number(feature.geometry.coordinates[1]),
                                    Number(feature.geometry.coordinates[0]),
                                ]}
								icon={icon}
                            >
								<Popup
                                    position={[
                                        Number(feature.geometry.coordinates[1]),
                                        Number(feature.geometry.coordinates[0]),
                                    ]}
                                >
                                    <div>
										<h2>
											{"Stop Name: " + feature.properties.stop_nm}
										</h2>
                                        <h2>
                                            {"Trip ID: " + feature.properties.trip_id}
                                        </h2>
										<h2>
											{"Arrival Time: " + feature.properties.arr_tm}
										</h2>
										<h2>
											{"Stop ID: " + feature.properties.stop_id}
										</h2>
										<h2>
											{"Stop Sequence: " + feature.properties.stop_sq}
										</h2>
										<h2>
											{"Stop Code: " + feature.properties.stop_cd}
										</h2>
                                    </div>
                                </Popup>
                            </Marker>
                        ))
                    }
			</MapContainer>
		);
  	}
}
