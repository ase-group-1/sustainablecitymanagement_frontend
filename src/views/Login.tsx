import React, { useEffect } from "react";
import { Navigate } from "react-router-dom";

import { LockClosedIcon } from "@heroicons/react/20/solid";
import { useAuth, useUserApi } from "../Auth";

// 243 to 149 lines.

export default function Login() {

    const { setAuth, auth } = useAuth();

    const { login } = useUserApi();

    const [ loading, setLoading ] = React.useState(false)
    const [ success, setSuccess ] = React.useState('')
    const [ error, setError ] = React.useState('')

    useEffect(() => {
        const authCheck = localStorage.getItem('scdAuth')
        
        if ( 'true' === authCheck ) {
            setAuth( true )
        }
    }, [setAuth]);

    const handleLoginFormSubmit = async ( e: React.ChangeEvent<HTMLFormElement> ) => {
        e.preventDefault()
        setLoading(true)
        const email = e.target.email ? e.target.email.value : ''
        const password = e.target.password ? e.target.password.value : ''
        if ( email && password ) {
            try {
                const user = await login(email, password);
                // console.log(user)
                setLoading(false)
                setSuccess('Login successful!')
            } catch (error: any) {
                setLoading(false)
                setError(error.response.data)
                // console.error("Error during login:", error);
            }
        }
    }

    if ( auth ) {
        return <Navigate to="/dashboard/analytics" replace />;
    }

    return (
        <div className="flex min-h-full items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
            <div className="w-full max-w-md space-y-8">
                <div>
                    <img
                        className="mx-auto h-12 w-auto"
                        src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
                        alt="Your Company"
                    />
                    <h2 className="mt-6 text-center text-3xl font-bold tracking-tight text-gray-900">
                        Sign in to your account
                    </h2>
                </div>
                <form
                    name="form"
                    aria-label="form"
                    aria-labelledby="form"
                    title="form"
                    className="mt-8 space-y-6"
                    action="#"
                    method="POST"
                    onSubmit={handleLoginFormSubmit}
                >
                    <input
                        type="hidden"
                        name="remember"
                        defaultValue="true"
                    />
                    <div className="-space-y-px rounded-md shadow-sm">
                        <div>
                            <label
                                htmlFor="email-address"
                                className="sr-only"
                            >
                                Email address
                            </label>
                            <input
                                id="email-address"
                                name="email"
                                type="email"
                                autoComplete="email"
                                required
                                className="relative block w-full appearance-none rounded-none rounded-t-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                                placeholder="Email address"
                                // onChange={(e) => handleEmailInput(e)}
                            />
                        </div>
                        <div>
                            <label
                                htmlFor="password"
                                className="sr-only"
                            >
                                Password
                            </label>
                            <input
                                id="password"
                                name="password"
                                type="password"
                                autoComplete="current-password"
                                required
                                className="relative block w-full appearance-none rounded-none rounded-b-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                                placeholder="Password"
                                // onChange={(e) => handlePasswordInput(e)}
                            />
                        </div>
                    </div>

                    <div className="flex items-center justify-between">
                        <div className="flex items-center">
                            <input
                                id="remember-me"
                                name="remember-me"
                                type="checkbox"
                                className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                            />
                            <label
                                htmlFor="remember-me"
                                className="ml-2 block text-sm text-gray-900"
                            >
                                Remember me
                            </label>
                        </div>

                        <div className="text-sm">
                            <a
                                href="/forgot-password"
                                className="font-medium text-indigo-600 hover:text-indigo-500"
                            >
                                Forgot your password?
                            </a>
                        </div>
                    </div>

                    <div>
                        <button
                            type="submit"
                            className="group relative flex w-full justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                        >
                            <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                                <LockClosedIcon
                                    className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                                    aria-hidden="true"
                                />
                            </span>
                            Sign in
                            {loading && <div className="inline-block ml-2 h-4 w-4 animate-spin rounded-full border-4 border-solid border-current border-r-transparent align-[-0.125em] motion-reduce:animate-[spin_1.5s_linear_infinite]" role="status">
                                <span className="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]">Loading...</span>
                            </div>}
                        </button>
                    </div>
                    <div>
                        {error && <div className="text-red-500 text-sm text-center">{error}</div>}
                    </div>
                    <div>
                        {success && <div className="text-green-500 text-sm text-center">{success}</div>}
                    </div>
                </form>
            </div>
        </div>
    );
}