import { Outlet, Link, useLocation, Navigate } from "react-router-dom";
import { ChevronDoubleRightIcon } from "@heroicons/react/20/solid";
import { useAuth } from "../Auth";

export default function Dashboard() {
    const { auth } = useAuth();

    let location = useLocation();

    console.log(location)

    if ( ! auth ) {
        return <div>You need to sign in</div>;
    }

    if ( '/dashboard' === location.pathname ) {
        return <Navigate to="/dashboard/analytics" replace={true} />
    }

    return (
        <div className="grid lg:grid-cols-4 gap-4 md:grid-cols-4 sm:grid-cols-2 App">
            <div className="col-span-1">
                <div className="justify-center">
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                        <ul className="divide-y divide-gray-200 rounded-md border border-gray-200">
                            <li className="flex items-center justify-between py-3 pl-3 pr-4 text-sm">
                                <div className="flex w-0 flex-1 items-center">
                                    <ChevronDoubleRightIcon
                                        className={`h-5 w-5 flex-shrink-0 ${
                                            "/dashboard/bike-stations" ===
                                            location.pathname
                                                ? "text-indigo-600"
                                                : "text-gray-400"
                                        }`}
                                        aria-hidden="true"
                                    />
                                    <Link
                                        to="/dashboard/bike-stations"
                                        relative="path"
                                    >
                                        <span className={`ml-2 w-0 flex-1 truncate ${
                                            "/dashboard/bike-stations" ===
                                            location.pathname
                                                ? "text-indigo-600"
                                                : "text-gray-400"
                                        }`}>
                                            Bike Stations
                                        </span>
                                    </Link>
                                </div>
                            </li>
                            <li className="flex items-center justify-between py-3 pl-3 pr-4 text-sm">
                                <div className="flex w-0 flex-1 items-center">
                                    <ChevronDoubleRightIcon
                                        className={`h-5 w-5 flex-shrink-0 ${
                                            "/dashboard/bus-routes" ===
                                            location.pathname
                                                ? "text-indigo-600"
                                                : "text-gray-400"
                                        }`}
                                        aria-hidden="true"
                                    />
                                    <Link
                                        to="/dashboard/bus-routes"
                                        relative="path"
                                    >
                                        <span className={`ml-2 w-0 flex-1 truncate ${
                                            "/dashboard/bus-routes" ===
                                            location.pathname
                                                ? "text-indigo-600"
                                                : "text-gray-400"
                                        }`}>
                                            Bus Routes
                                        </span>
                                    </Link>
                                </div>
                            </li>
                            <li className="flex items-center justify-between py-3 pl-3 pr-4 text-sm">
                                <div className="flex w-0 flex-1 items-center">
                                    <ChevronDoubleRightIcon
                                        className={`h-5 w-5 flex-shrink-0 ${
                                            "/dashboard/bus-reroutes" ===
                                            location.pathname
                                                ? "text-indigo-600"
                                                : "text-gray-400"
                                        }`}
                                        aria-hidden="true"
                                    />
                                    <Link
                                        to="/dashboard/bus-reroutes"
                                        relative="path"
                                    >
                                        <span className={`ml-2 w-0 flex-1 truncate ${
                                            "/dashboard/bus-reroutes" ===
                                            location.pathname
                                                ? "text-indigo-600"
                                                : "text-gray-400"
                                        }`}>
                                            Re-routes
                                        </span>
                                    </Link>
                                </div>
                            </li>
                            <li className="flex items-center justify-between py-3 pl-3 pr-4 text-sm">
                                <div className="flex w-0 flex-1 items-center">
                                    <ChevronDoubleRightIcon
                                        className={`h-5 w-5 flex-shrink-0 ${
                                            "/dashboard/dart-stations" ===
                                            location.pathname
                                                ? "text-indigo-600"
                                                : "text-gray-400"
                                        }`}
                                        aria-hidden="true"
                                    />
                                    <Link
                                        to="/dashboard/dart-stations"
                                        relative="path"
                                    >
                                        <span className={`ml-2 w-0 flex-1 truncate ${
                                            "/dashboard/dart-stations" ===
                                            location.pathname
                                                ? "text-indigo-600"
                                                : "text-gray-400"
                                        }`}>
                                            Dart Stations
                                        </span>
                                    </Link>
                                </div>
                            </li>
                            <li className="flex items-center justify-between py-3 pl-3 pr-4 text-sm">
                                <div className="flex w-0 flex-1 items-center">
                                    <ChevronDoubleRightIcon
                                        className={`h-5 w-5 flex-shrink-0 ${
                                            "/dashboard/luas-stations" ===
                                            location.pathname
                                                ? "text-indigo-600"
                                                : "text-gray-400"
                                        }`}
                                        aria-hidden="true"
                                    />
                                    <Link
                                        to="/dashboard/luas-stations"
                                        relative="path"
                                    >
                                        <span className={`ml-2 w-0 flex-1 truncate ${
                                            "/dashboard/luas-stations" ===
                                            location.pathname
                                                ? "text-indigo-600"
                                                : "text-gray-400"
                                        }`}>
                                            Luas Stations
                                        </span>
                                    </Link>
                                </div>
                            </li>
                            <li className="flex items-center justify-between py-3 pl-3 pr-4 text-sm">
                                <div className="flex w-0 flex-1 items-center">
                                    <ChevronDoubleRightIcon
                                        className={`h-5 w-5 flex-shrink-0 ${
                                            "/dashboard/footfall" ===
                                            location.pathname
                                                ? "text-indigo-600"
                                                : "text-gray-400"
                                        }`}
                                        aria-hidden="true"
                                    />
                                    <Link
                                        to="/dashboard/footfall"
                                        relative="path"
                                    >
                                        <span className={`ml-2 w-0 flex-1 truncate ${
                                            "/dashboard/footfall" ===
                                            location.pathname
                                                ? "text-indigo-600"
                                                : "text-gray-400"
                                        }`}>
                                            Footfall
                                        </span>
                                    </Link>
                                </div>
                            </li>
                        </ul>
                    </dd>
                </div>
            </div>
            <div className="col-span-1 lg:col-span-3 md:col-span-3 sm:col-span-1">
                <Outlet />
            </div>
        </div>
    );
}
