import React from "react";
import axios from "axios";
import geojson from "../data/shapes_gdf.json";
import gtfs from "../gtfs.json";

import Loader from "../components/Loader";
import BikeStations from "../components/BikeStations";
import BusRoutes from "../components/BusRoutes";
import BusStops from "../components/BusStops";

// https://data.smartdublin.ie/dublinbikes-api/last_snapshot/
// console.log(geoJson.features[0].properties.route_short_name);

// @TODO
// 1. Add tests
// 2. colour code paths or somehow show directions
// 3. Simplify code / break into components
// 4. Change map center on bus route selection

// 1004 lines of code to 246

const views = ["Bikes", "Buses", "Bus Stops"];

const dataOne: GeoJSON.Feature = {
    type: "Feature",
    properties: {
        agency_name: "Dublin Bus",
        route_id: "60-53-b12-1",
        agency_id: "978",
        route_short_name: "53",
        route_type: 3,
    },
    geometry: {
        type: "LineString",
        coordinates: [
            [-6.21697, 53.352],
            [-6.22103, 53.35215],
            [-6.22285, 53.3523],
            [-6.22284, 53.35241],
            [-6.22482, 53.35243],
            [-6.22733, 53.35387],
            [-6.22922, 53.355],
            [-6.22991, 53.3544],
            [-6.23216, 53.35257],
            [-6.23236, 53.3526],
            [-6.23249, 53.35256],
            [-6.23336, 53.35192],
            [-6.23365, 53.35176],
            [-6.23394, 53.35169],
            [-6.23445, 53.3517],
            [-6.23535, 53.35178],
            [-6.23583, 53.35186],
            [-6.23605, 53.35193],
            [-6.23784, 53.35271],
            [-6.23581, 53.35437],
            [-6.23688, 53.35482],
            [-6.2371, 53.35487],
            [-6.23734, 53.35486],
            [-6.23742, 53.35483],
            [-6.2376, 53.3547],
            [-6.23778, 53.35451],
            [-6.23885, 53.355],
            [-6.23877, 53.35547],
            [-6.23866, 53.35579],
            [-6.23843, 53.35624],
            [-6.23801, 53.35678],
            [-6.23763, 53.35722],
            [-6.23725, 53.35751],
            [-6.23534, 53.35852],
            [-6.23853, 53.36036],
            [-6.23893, 53.36039],
            [-6.23929, 53.36034],
            [-6.23972, 53.35992],
            [-6.24115, 53.35861],
            [-6.24211, 53.35795],
            [-6.24483, 53.35616],
            [-6.24619, 53.35514],
            [-6.24668, 53.35473],
            [-6.24735, 53.35407],
            [-6.24964, 53.352],
            [-6.2499, 53.35166],
            [-6.25, 53.35146],
            [-6.25102, 53.35127],
            [-6.2535, 53.35085],
        ],
    },
};

type Geo = {
    type: string;
    features: any;
};

type Gtfs = {
    type: string;
    crs: any;
    features: any;
};

const geoJson = geojson as Geo;
const gtfsObj = gtfs as Gtfs;

class App extends React.Component {
    state = {
        view: "Bikes",
        defaultCenter: { lat: 53.3548, lng: -6.2603 },
        defaultZoom: 13,
        busNumbers: ["1", "11", "116", "118", "120", "122", "123"],
        bikesData: [],
        busStopsData: [],
        busRoute: "53",
        busData: dataOne,
        projectedBikes: 0,
        routes: {
            type: "FeatureCollection",
            features: [],
        },
    };
    componentDidMount = () => {
        console.log("app mounted");
        console.log(process.env.REACT_APP_BIKES_DATA_API);
        const busStopsData = gtfsObj.features;
        this.fetchBikesData(process.env.REACT_APP_BIKES_DATA_API as string);
        this.setState({
            busStopsData,
            routes: {
                type: "FeatureCollection",
                features: this.filterRoutes("53"),
            },
        });
    };
    fetchBikesData = async (apiUrl: string) => {
        let $this = this;
        await axios
            .get(apiUrl)
            .then((response) => {
                console.log(`Bikes API Request Status: ${response.status}`);
                if (200 === response.status) {
                    $this.setState({
                        bikesData: response.data,
                    });
                }
            })
            .catch(function (error) {
                console.log(error.toJSON());
            });
    };
    filterRoutes = (route: string) => {
        console.log("filters...");
        var routes = geoJson.features.filter(function (item: any) {
            if (route === item.properties.route_short_name) {
                return item;
            }
            return false;
        });
        return routes;
    };
    getBusRouteOptions = () => {
        const unique = Array.from(
            new Set(
                geoJson.features.map((item: any) => {
                    if (item.properties.route_short_name) {
                        return item.properties.route_short_name;
                    } else if (item.properties.routes) {
                        return item.properties.routes[0].route_short_name;
                    } else {
                        return null;
                    }
                })
            )
        );
        const options = unique.map((route: any) => {
            return (
                <option key={route} value={route}>
                    {route}
                </option>
            );
        });
        return options;
    };
    selectBusRoutes = () => {
        return (
            <>
                <label
                    htmlFor="country"
                    className="block text-sm font-medium text-gray-700"
                >
                    Select a route
                </label>
                <select
                    id="route"
                    name="route"
                    className="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                    onChange={(e) => this.changeBusRoute(e)}
                >
                    {this.getBusRouteOptions()}
                </select>
            </>
        );
    };
    changeBusRoute = async (event: React.ChangeEvent<HTMLSelectElement>) => {
        const features = await this.filterRoutes(event.target.value);
        this.setState({
            busRoute: event.target.value,
            routes: {
                type: "FeatureCollection",
                features: features,
            },
        });
    };
    changeView = (event: React.ChangeEvent<HTMLSelectElement>) => {
        console.log(`view: ` + event.target.value);
        console.log(this.state);
        this.setState({
            view: event.target.value,
        });
    };
    render = () => {
        let view;
        if (this.state.bikesData.length && "Bikes" === this.state.view) {
            view = (
                <BikeStations
                    bikesData={this.state.bikesData}
                    projectedBikes={this.state.projectedBikes}
                />
            );
        } else if ("Buses" === this.state.view) {
            view = (
                <BusRoutes
                    defaultCenter={this.state.defaultCenter}
                    defaultZoom={this.state.defaultZoom}
                    busRoute={this.state.busRoute}
                    routes={this.state.routes}
                />
            );
        } else if ("Bus Stops" === this.state.view) {
            view = <BusStops busStopsData={this.state.busStopsData} />;
        } else {
            view = <Loader />;
        }
        return (
            <div className="grid lg:grid-cols-4 gap-4 md:grid-cols-12 App">
                <div className="col-span-1">
                    <div className="justify-center">
                        <div className="mb-3">
                            <label
                                htmlFor="country"
                                className="block text-sm font-medium text-gray-700"
                            >
                                Select a view
                            </label>
                            <select
                                id="country"
                                name="country"
                                autoComplete="country-name"
                                className="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                                onChange={(e) => this.changeView(e)}
                            >
                                {views.length &&
                                    views.map((view) => (
                                        <option key={view} value={view}>
                                            {view}
                                        </option>
                                    ))}
                            </select>
                        </div>
                        <div className="mb-3">
                            {"Buses" === this.state.view &&
                                this.selectBusRoutes()}
                        </div>
                    </div>
                </div>
                <div className="col-span-3">{view}</div>
            </div>
        );
    };
}

export default App;
