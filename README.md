# Sustainable City Dashboard Frontend application


To run the React frontend application.
Enter the following command using the terminal inside the project directory.

`npm install`

`npm start`

We have deployed the front end application on Netlify as it gives us a free hosting option.

The domain we have deployed it on is : https://dublincitydashboard.netlify.app/
