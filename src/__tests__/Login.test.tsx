// import React from 'react';
import { render, screen, waitFor } from '@testing-library/react'
import user from '@testing-library/user-event'
// import userEvent from '@testing-library/user-event'
// import axios from "axios";
import Login from '../views/Login';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import Root from '../routes/root';
import Dashboard from '../views/Dashboard';
import { AuthContext } from '../Auth';
import Bikes from '../views/Bikes';

// let container: any;

test('1: shows login page by default', async () => {
  render(<Login />);

  const textElement = screen.getByText(/Sign in to your account/i);
  expect(textElement).toBeInTheDocument();
});

test('2: login form on login page with no auth', async () => {
  render(<Login />);

  const inputEmail = screen.getByRole('textbox')
  expect(inputEmail).toHaveAccessibleName('Email address')

  // password has no implicit role so cant use getAllRoleBy for it. ref: https://github.com/testing-library/dom-testing-library/issues/567
  // const inputs = screen.getAllByRole('textbox')
  // expect(inputs).toHaveLength(2)

  const inputPassword = screen.getByLabelText('Password')
  expect(inputPassword).toHaveAccessibleName('Password')

  const button = screen.getByRole('button')
  expect(button).toHaveAccessibleName('Sign in')

  // const button = screen.getByRole('button')
  // await user.click(button)

  // await waitFor(async () => {
  //   const textElement = screen.getByText(/Sign in to your account/i);
  //   expect(textElement).toBeInTheDocument();
  // }) 
});

test('3: login form submit', async () => {
  // const mock = jest.fn()

  render(
  <AuthContext.Provider value={{auth:false, setAuth: () => {}, user: false}}>
    <MemoryRouter initialEntries={['/login']}>
      <Routes>
        <Route path="/" element={<Root />}>
          <Route path="/login" element={<Login />} />
          <Route path="/dashboard" element={<Dashboard />} />
        </Route>
      </Routes>
    </MemoryRouter>
  </AuthContext.Provider>
  );

  // find inputs and button.
  const email    = screen.getByRole('textbox')
  const password = screen.getByLabelText('Password')
  const button   = screen.getAllByRole('button')[2]

  // simulate typing in an email.
  user.click(email)
  user.keyboard('test@test.com')
  
  // simulate typing in a password.
  user.click(password)
  user.keyboard('test')

  // simulate button click.
  await user.click(button)

  await waitFor(async () => {
    const textElement = screen.getByText(/Sign in to your account/i);
    expect(textElement).toBeInTheDocument();
  })
});

// test('login 4: login form submit with auth', async () => {
//   render(
//   <AuthContext.Provider value={{auth:true, setAuth: () => {}, user: true}}>
//     <MemoryRouter initialEntries={['/login']}>
//       <Routes>
//         <Route path="/" element={<Root />}>
//           <Route path="/login" element={<Login />} />
//           <Route path="/dashboard" element={<Dashboard />}>
//             <Route path="/dashboard/bike-stations" element={<Bikes />} />
//           </Route>
//         </Route>
//       </Routes>
//     </MemoryRouter>
//   </AuthContext.Provider>
//   );

//   const textElement = screen.getByText(/Dashboard/i);
//   expect(textElement).toBeInTheDocument();
// });