import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import axios from 'axios';
import Bikes from '../views/Bikes';
import BikeStations from '../components/BikeStations';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe('Bikes', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    test('fetches bike data from the API', async () => {
        const bikesDataMock = [
            { id: 1, name: 'Station 1', availableBikes: 3, lat: "53.3548", lon: "-6.2603", longi: "-6.2603", avl_bks: 3, bks_std: 10, sta: 'Active', addr_ln: 'Station 1' },
            { id: 2, name: 'Station 2', availableBikes: 5, lat: "53.3548", lon: "-6.2603", longi: "-6.2603", avl_bks: 5, bks_std: 10, sta: 'Active', addr_ln: 'Station 1'  },
        ];
        const axiosResponse = {
            data: bikesDataMock,
            status: 200,
        };
        mockedAxios.get.mockResolvedValue(axiosResponse);

        render(<Bikes />);

        await waitFor(() => {
            expect(mockedAxios.get).toHaveBeenCalledTimes(1);
        });
        expect(mockedAxios.get).toHaveBeenCalledWith(
            process.env.REACT_APP_BIKES_DATA_API as string,
        );
    });

    test('renders BikeStations component with correct props after fetching bike data', async () => {
        const bikesDataMock = [
            { id: 1, name: 'Station 1', availableBikes: 3, lat: "53.3548", lon: "-6.2603", longi: "6.2603", avl_bks: 3, bks_std: 10, sta: 'Active', addr_ln: 'Station 1' },
            { id: 2, name: 'Station 2', availableBikes: 5, lat: "53.3548", lon: "-6.2603", longi: "6.2603", avl_bks: 5, bks_std: 10, sta: 'Active', addr_ln: 'Station 1'  },
        ];
        const axiosResponse = {
            data: bikesDataMock,
            status: 200,
        };
        mockedAxios.get.mockResolvedValue(axiosResponse);

        render(<Bikes />);

        await waitFor(() => {
            expect(screen.getByTestId('bike-stations')).toBeInTheDocument();
        });

        // expect(screen.getByTestId('bike-stations')).toHaveAttribute(
        //     'bikesData',
        //     JSON.stringify(bikesDataMock),
        // );
        // expect(screen.getByTestId('bike-stations')).toHaveAttribute(
        //     'projectedBikes',
        //     '0',
        // );
    });

    test('handles API request error', async () => {
        const axiosError = new Error('API request error');
        mockedAxios.get.mockRejectedValue(axiosError);

        console.log = jest.fn();
        render(<Bikes />);

        await waitFor(() => {
            expect(mockedAxios.get).toHaveBeenCalledTimes(1);
        });
        // expect(mockedAxios.get).toHaveBeenCalledWith(
        //     process.env.REACT_APP_BIKES_DATA_API as string,
        // );
        // expect(console.log).toHaveBeenCalledWith(axiosError.toJSON());
    });
});

