import * as React from "react";
import { Marker, Polyline, Popup } from "react-leaflet";

import iconImage from '../images/dart.png'
import iconRetinaImage from '../images/dart-2x.png'
import markerShadow from '../images/marker-shadow.png'

import L, { LatLngExpression } from "leaflet";

let icon = L.icon({
	iconUrl: iconImage,
	iconRetinaUrl: iconRetinaImage,
	shadowUrl: markerShadow,
	iconSize: [32, 32],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	tooltipAnchor: [16, -28],
	shadowSize: [41, 41],
});

export interface IDartRoutesProps {
	lat: string,
	longi: string,
    code: string,
    polyline: LatLngExpression[][]
}

const limeOptions = { color: 'lime' }

export default class DartRoutes extends React.Component<IDartRoutesProps> {

    render() {
        // console.log(`live train props :`);
		// console.log(this.props);

		const { lat, longi, code, polyline } = this.props;

        return (
            <>
                <Marker
                    key={Math.random()}
                    position={[
                        Number(lat),
                        Number(longi),
                    ]}
                    icon={icon}
                >
                    <Popup
                        position={[
                            Number(lat),
                            Number(longi),
                        ]}
                    >
                        <div>
                            <h2>{"Train Code: " + code}</h2>
                        </div>
                    </Popup>
                </Marker>
                <Polyline pathOptions={limeOptions} positions={polyline} />
            </>
        );
    }
}
