import React, { useEffect } from "react";
import L, { LatLngExpression } from "leaflet";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";

import iconImage from '../images/pin-blue.png'
import iconRetinaImage from '../images/pin-blue-2x.png'
import markerShadow from '../images/marker-shadow.png'
import DartRoutes from "./DartRoutes";
import Select from 'react-select'
import TrafficDensity from "./TrafficDensity";

let icon = L.icon({
  iconUrl: iconImage,
  iconRetinaUrl: iconRetinaImage,
  shadowUrl: markerShadow,
  iconSize: [35, 35],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41],
});

type IDartStationsDataProps = {
	stn_desc: string,
	srvr_ts: string,
	trn_cd: string,
	stn_nm: string,
	stn_cd: string,
	qry_ts: string,
	trn_dt: string,
	org_nm: string,
	destn: string,
	org_ts: string,
	destn_ts: string,
	sta: string,
	lst_loc: string,
	due_ts: string,
	late: string,
	exp_arr: string,
	exp_dep: string,
	sch_arr: string,
	sch_dep: string,
	drtn: string,
	trn_typ: string,
	loc_typ: string
}

interface IDartStationsProps {
	dartsData: any[],
	dartStationsData: Array<IDartStationsDataProps>,
	dartLiveData: any[]
}

export default function DartStations(props:IDartStationsProps) {
	const [ liveTrain, setLiveTrain ] = React.useState([])
	const [ liveTrainData, setLiveTrainData ] = React.useState([])
	const [ polyline, setPolyline ] = React.useState<LatLngExpression[][]>([])

	useEffect(() => {
		const liveTrainT:any = props.dartLiveData && Array.isArray(props.dartLiveData) ? props.dartLiveData.filter( (train) => train.TrainCode === liveTrain ) : []
		setLiveTrainData( liveTrainT )
		if ( liveTrainT.length && liveTrainT[0] && liveTrainT[0].TrainLatitude && liveTrainT[0].TrainLongitude ) {
			const polylineLineVal = [ liveTrainT[0].TrainLatitude, liveTrainT[0].TrainLongitude ];
			const polylineLineLn = polyline.length;
			const polylineOldVal:LatLngExpression[] = polyline[+polylineLineLn-1];

			if ( polylineLineLn ) {
				if ( liveTrainT[0] && polylineOldVal[0] && polylineOldVal[0] !== liveTrainT[0].TrainLatitude && polylineOldVal[1] !== liveTrainT[0].TrainLongitude ) {
					setPolyline( polyline => [ ...polyline, polylineLineVal ] )
				}
			} else {
				setPolyline( polyline => [ ...polyline, polylineLineVal ] )
			}
		}
	}, [liveTrain, polyline, props.dartLiveData]);

	function handleTrainChange(event: any) {
		setPolyline( [] )
		setLiveTrain( event.value )
	}

	return (
        <>
			<Select className="react-select-container" options={props.dartLiveData && props.dartLiveData.length ? props.dartLiveData.map( (train) => {
				return {
					value: train.TrainCode,
					label: train.TrainCode
				}
			} ) : []} placeholder="Select a running train" onChange={handleTrainChange} menuPortalTarget={document.body} styles={{
				control: base => ({ ...base, marginBottom: 20 }),
				menuPortal: base => ({ ...base, zIndex: 9999 }),
				input: base => ({ ...base, boxShadow: 'none' }),
			}} />
			<div id="map">
				<MapContainer
				center={[53.3548, -6.2603]}
				zoom={11}
				scrollWheelZoom={true}
				>
					<TileLayer
						attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
						url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
					/>

					<TrafficDensity />

					{
					props.dartsData.length && props.dartsData.length &&
						props.dartsData.map((dartStation) => {
							const stationData: any = props.dartStationsData.filter( item => {
								return dartStation["stn_cd"] === item.stn_cd
							})
							const stationDataInfo = stationData[0] ? stationData[0] : null
							const trainCode = stationDataInfo && stationDataInfo["trn_cd"] ? stationDataInfo["trn_cd"] : null
							const expectedArrival = stationDataInfo && stationDataInfo["exp_arr"] ? stationDataInfo["exp_arr"] : null
							const expectedDeparture = stationDataInfo && stationDataInfo["exp_dep"] ? stationDataInfo["exp_dep"] : null

							return <Marker
								key={dartStation["stn_id"]}
								position={[
								Number(dartStation["lat"]),
								Number(dartStation["longi"]),
								]}
								icon={ icon }
							>
								<Popup
								position={[
									Number(dartStation["lat"]),
									Number(dartStation["longi"]),
								]}
								>
									<div>
										<h2>{"Station Name: " + dartStation["stn_desc"]}</h2>
										<h2>{"Station Code: " + dartStation["stn_cd"]}</h2>
										{
											trainCode && expectedArrival && expectedDeparture && <>
												<h2>{"Train Code: " + trainCode }</h2>
												<h2>{"Expected Arrival: " + expectedArrival }</h2>
												<h2>{"Expected Departure: " + expectedDeparture }</h2>
											</>
										}
										{
											! trainCode && ! expectedArrival && ! expectedDeparture && 
												<h2 className="text-red-600 font-bold">No Trains Scheduled</h2>
												
										}
									</div>
								</Popup>
							</Marker>
						})
					}

					{liveTrainData && liveTrainData[0] && <DartRoutes lat={liveTrainData[0]['TrainLatitude']} longi={liveTrainData[0]['TrainLongitude']} code={liveTrainData[0]['TrainCode']} polyline={polyline} />}
					
				</MapContainer>
			</div>
		</>
    );
}