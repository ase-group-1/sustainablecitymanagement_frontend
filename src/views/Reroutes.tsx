import React, { useEffect } from "react";
import { MapContainer, TileLayer, Polygon, Popup, Polyline, GeoJSON } from "react-leaflet";
import { GeoJsonObject } from 'geojson';
import axios from "axios";
import geojson from "../data/shapes_gdf.json";

type Geo = {
    type: string;
    features: any;
};

const geoJson = geojson as Geo;

export default function Reroutes() {

    const [ mapOptions, setMapOptions ]  = React.useState( { defaultCenter: { lat: 53.3548, lng: -6.2603 }, defaultZoom: 13 })
    const [ polygons, setPolygons ]      = React.useState([])
    const [ routes, setRoutes ]          = React.useState<any>([])
    const [ actualBusRoute, setActualBusRoute ] = React.useState("")
    const [ actualRoutes, setActualRoutes ] = React.useState({ type: "FeatureCollection", features: [] })
    const [ greenRoute, setGreenRoutes ] = React.useState([])

    useEffect(() => {
        async function fetchRoutesData(id: string) {
            const apiUrl = process.env.REACT_APP_REROUTE_ROUTES_API + `?deviceid=` + id as string
            console.log(apiUrl)
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(`Polygon Reroute Request Status: ${response.status}`);
                    if (200 === response.status) {
						const resp = response.data[0]
                        setRoutes((current: any) => {
                            const existingItem = current.find((item: any) => item.id === id);
                          
                            if (existingItem) {
                              // If an existing item with the same ID is found, return the current array unchanged
                              return current;
                            } else {
                              // If an existing item with the same ID is not found, add the new item to the array
                              return [...current, {
                                id: id,
                                data: resp
                              }];
                            }
                        });                          
                    }
                })
                .catch(function (error) {
                    console.log(error)
                });
        };
        async function fetchPolygonData(apiUrl: string) {
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(`Polygons API Request Status: ${response.status}`);
                    console.log(response.data[0])
                    if (200 === response.status) {
                        setPolygons( response.data[0] );
                        Object.keys(response.data[0]).map((key, index) => {
                            console.log(key)
                            fetchRoutesData(key)
                            return null
                        })
                    }
                })
                .catch(function (error) {
                    console.log(error.toJSON());
                });
        };
        fetchPolygonData( process.env.REACT_APP_REROUTE_POLYGON_API as string )
    }, []);

    const handleSelectedRoute = (route: any, key: any) => {
        console.log(route)
        setGreenRoutes(route.final_route)
        handleActualBusRoute(key)
    }

    function filterActualRoutes(route: string) {
        var routesData = geoJson.features.filter(function (item: any) {
            if (route === item.properties.route_short_name) {
                return item;
            }
            return false;
        });
        return routesData;
    };

    function handleActualBusRoute(route: string) {
        setActualBusRoute(route)
        const filteredRoutes = filterActualRoutes( route )
        setActualRoutes( { type: "FeatureCollection", features: filteredRoutes } )
    }

    return (
        <MapContainer key={'reroute'} center={mapOptions.defaultCenter} zoom={mapOptions.defaultZoom}>
            <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" attribution="&copy; <a href=&quot;https:www.openstreetmap.org/copyright&quot;>OpenStreetMap</a> contributors" />
            {
                Object.keys(polygons).map((key, index) => {
                    const polygon = polygons[key as any]
                    return <Polygon key={key} pathOptions={{ color: 'purple' }} positions={polygon}>
                        <Popup>
                            {
                                routes.map((route: any, index: number) => {
                                    if (route.id === key) {
                                        return <div key={index}>
                                            <h2 style={{fontWeight:'bold',marginBottom:10}}>Select a route:</h2>
                                            {
                                                Object.keys(route.data).map((key, index) => {
                                                    const final_route = route.data[key]
                                                    return <h3 key={index} onClick={() => handleSelectedRoute(final_route, key)} style={{marginBottom:5,cursor:'pointer'}}>{key}</h3>
                                                })
                                            }
                                        </div>
                                    }
                                    return null
                                })
                            }
                        </Popup>
                    </Polygon>
                })
            }
            <Polyline pathOptions={{ color: '#32CD32' }} positions={greenRoute} />
            <GeoJSON key={actualBusRoute} data={actualRoutes as GeoJsonObject} style={{ color: '#FF6347' }} />
        </MapContainer>
    );
}
