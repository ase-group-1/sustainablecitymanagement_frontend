import { BrowserRouter, MemoryRouter, Route, Routes } from 'react-router-dom'
import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import Root from '../routes/root'
import Login from '../views/Login'
import Dashboard from '../views/Dashboard'

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "/"
  })
}));

test('4: router test - homepage', async () => {
  render(<Root />, {wrapper: BrowserRouter});

  const textElement = screen.getByText(/Making Dublin Sustainable One Transport At A Time/i);
  expect(textElement).toBeInTheDocument();
});

test('5: redirect from home to login page', async () => {
  render(
  <MemoryRouter initialEntries={['/']}>
    <Routes>
      <Route path="/" element={<Root />}>
        <Route path="/login" element={<Login />} />
      </Route>
    </Routes>
  </MemoryRouter>
  );

  const user = userEvent.setup()

  const button = screen.getAllByRole('link')[1]
  expect(button).toHaveAccessibleName('Login')

  await user.click(button)

  await waitFor(async () => {
    const textElement = screen.getByText(/Sign in to your account/i);
    expect(textElement).toBeInTheDocument();
  })
});

test('6: test login', async () => {
  render(
  <MemoryRouter initialEntries={['/']}>
    <Routes>
      <Route path="/" element={<Root />}>
        <Route path="/login" element={<Login />} />
        <Route path="/dashboard" element={<Dashboard />} />
      </Route>
    </Routes>
  </MemoryRouter>
  );

  const user = userEvent.setup()

  const button = screen.getAllByRole('link')[1]
  expect(button).toHaveAccessibleName('Login')

  await user.click(button)

  await waitFor(async () => {
    const textElement = screen.getByText(/Sign in to your account/i);
    expect(textElement).toBeInTheDocument();
  })

  // find inputs and button.
  const email    = screen.getByRole('textbox')
  const password = screen.getByLabelText('Password')
  const loginBtn = screen.getByText('Sign in')

  // simulate typing in an email.
  user.click(email)
  user.keyboard('test@test.com')
  
  // simulate typing in a password.
  user.click(password)
  user.keyboard('test')

  // jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem')
  // Object.setPrototypeOf(window.localStorage.setItem, jest.fn())

  // simulate button click.
  await user.click(loginBtn)
});