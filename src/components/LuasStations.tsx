import React from "react";
import L from "leaflet";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";

import iconImage from '../images/pin-blue.png'
import iconRetinaImage from '../images/pin-blue-2x.png'
import markerShadow from '../images/marker-shadow.png'

import axios from "axios";
import TrafficDensity from "./TrafficDensity";

let icon = L.icon({
  iconUrl: iconImage,
  iconRetinaUrl: iconRetinaImage,
  shadowUrl: markerShadow,
  iconSize: [35, 35],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41],
});

type ILuasStationsDataProps = {
	abrev: string,
	isParkRide: string,
	isCycleRide: string,
	lat: string,
	longi: string,
	pronunciation: string,
	stpnm: string,
	lineType: string,
}

interface ILuasStationsProps {
	luasData: any[],
}

export default function LuasStations(props:ILuasStationsProps) {

	function handleStationData(event: any, station: ILuasStationsDataProps) {
		async function fetchLuasData(apiUrl: string) {
			let popup = event.target.getPopup()
			popup.setContent( "Loading..." );
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(`Luas Station Request Status: ${response.status}`);
                    if (200 === response.status) {
						const stationData = response.data
						let popupdata = `<div>
							<h2>${"Station Name: " + station.stpnm}</h2>
							<h2>${"Station Abrev: " + station.abrev}</h2>
							<h2>${"Line Type: " + station.lineType}</h2>
							<h2>${"Is Park Ride: " + station.isParkRide}</h2>
							<h2>${"Is Cycle Ride: " + station.isCycleRide}</h2>`

						popupdata += stationData.length && stationData.map( (data: { due: string, direction: string, destination: string }) => {
							return `<br><hr><br>
								<h2>${"Due: " + data.due}</h2>
								<h2>${"Direction: " + data.direction}</h2>
								<h2>${"Destination: " + data.destination}</h2>`
						})
						popupdata += `</div>`
   						popup.setContent( popupdata.replace( /,/g, '' ) );
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        };
        fetchLuasData( process.env.REACT_APP_LUAS_STATION_STATUS_DATA_API + `?station=` + station.abrev as string )
	}

	return (
        <>
			<div id="map">
				<MapContainer
				center={[53.3548, -6.2603]}
				zoom={11}
				scrollWheelZoom={true}
				>
					<TileLayer
						attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
						url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
					/>

					<TrafficDensity />

					{
					props.luasData.length && props.luasData.length &&
						props.luasData.map((luasStation, index) => {
							return <Marker
								key={luasStation["abrev"]}
								position={[
								Number(luasStation["lat"]),
								Number(luasStation["longi"]),
								]}
								icon={ icon }
								eventHandlers={{
									click: (e) => {
									  handleStationData(e,luasStation)
									},
								}}
							>
								<Popup
								position={[
									Number(luasStation["lat"]),
									Number(luasStation["longi"]),
								]}
								></Popup>
							</Marker>
						})
					}
					
				</MapContainer>
			</div>
		</>
    );
}
