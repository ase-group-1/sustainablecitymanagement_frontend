import React, { useEffect } from "react";
import axios from "axios";

// import Loader from "./components/Loader";
import LuasStations from "../components/LuasStations";

// var parser = require('xml2json-light');

export default function Luas() {

    const [ luasData, setLuasData ] = React.useState([])

    useEffect(() => {
        async function fetchLuasData(apiUrl: string) {
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(`Darts Data Request Status: ${response.status}`);
                    if (200 === response.status) {
                        console.log(response.data)
                        setLuasData( response.data );
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        };
        fetchLuasData( process.env.REACT_APP_LUAS_STATIONS_LIST_DATA_API as string )
    }, []);

    return (
        <LuasStations luasData={luasData} />
    );
}
