import React, { useEffect } from "react";
import axios from "axios";

// import Loader from "./components/Loader";
import DartStations from "../components/DartStations";

// var parser = require('xml2json-light');

export default function Darts() {

    const [ dartsData, setDartsData ] = React.useState([])
    const [ dartStationsData, setDartStationsData ] = React.useState([])
    const [ dartLiveData, setDartLiveData ] = React.useState([])

    useEffect(() => {
        async function fetchDartsData(apiUrl: string) {
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(`Darts Data Request Status: ${response.status}`);
                    console.log(response.data)
                    if (200 === response.status) {
                        setDartsData( response.data );
                    }
                })
                .catch(function (error) {
                    console.log(error.toJSON());
                });
        };
        async function fetchDartStationsData(apiUrl: string) {
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(`Dart Stations Request Status: ${response.status}`);
                    console.log(response.data)
                    if (200 === response.status) {
                        setDartStationsData( response.data );
                    }
                })
                .catch(function (error) {
                    console.log(error.toJSON());
                });
        };
        async function fetchDartLiveData(apiUrl: string) {
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(`Dart Live Stations Request Status: ${response.status}`);
                    console.log(response.data)
                    if (200 === response.status) {
                        setDartLiveData( response.data );
                    }
                })
                .catch(function (error) {
                    console.log(error.toJSON());
                });
        };
        fetchDartsData( process.env.REACT_APP_DARTS_DATA_API as string )
        fetchDartStationsData( process.env.REACT_APP_DART_STATIONS_DATA_API as string )
        fetchDartLiveData( process.env.REACT_APP_DART_LIVE_DATA_API as string )

        const interval = setInterval(() => {
            fetchDartLiveData( process.env.REACT_APP_DART_LIVE_DATA_API as string )
        }, 30000);
        return () => clearInterval(interval);
    }, []);

    return (
        <DartStations dartsData={dartsData} dartStationsData={dartStationsData} dartLiveData={dartLiveData} />
    );
}
