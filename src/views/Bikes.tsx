import React, { useEffect } from "react";
import axios from "axios";

import BikeStations from "../components/BikeStations";
import bP from "../data/bikes_predict.json";
import bD from "../data/dublin_bike.json";

export default function Bikes() {

    const [ bikesData, setBikesData ] = React.useState([])
    const [ bikesPredictionData, setBikesPredictionData ] = React.useState([])
    const [ projectedBikes ] = React.useState(0)

    useEffect(() => {
        async function fetchBikesData(apiUrl: string) {
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(`Bikes API Request Status: ${response.status}`);
                    if (200 === response.status) {
                        setBikesData( response.data );
                    }
                })
                .catch(function (error) {
                    // console.log(error.toJSON());
                });
        };
        // fetchBikesData( process.env.REACT_APP_BIKES_PREDICT_DATA_API as string )
        setBikesData( bD as any );
        setBikesPredictionData( bP as any );
    }, []);

    return (
        <BikeStations
            bikesData={bikesData}
            bikesPredictionData={bikesPredictionData}
            projectedBikes={projectedBikes}
        />
    );
}
