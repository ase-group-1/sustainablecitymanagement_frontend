import React from "react";
import { render, screen } from "@testing-library/react";
import Analytics from "../views/Analytics";

describe("Analytics Component", () => {
  beforeEach(() => {
    jest.spyOn(console, "log").mockImplementation(() => {});
    jest.spyOn(console, "error").mockImplementation(() => {});
  });

  afterEach(() => {
    // console.log.mockRestore();
    // console.error.mockRestore();
  });

  test("should render Bike Stations and low/high bikes button", async () => {
    render(<Analytics />);
    const bikeStations = screen.getByText(/Bike Stations/i);
    expect(bikeStations).toBeInTheDocument();
    const lowBikesButton = screen.getByText(/Low:/i);
    expect(lowBikesButton).toBeInTheDocument();
    const highBikesButton = screen.getByText(/High:/i);
    expect(highBikesButton).toBeInTheDocument();
  });

  test("should render Street Cameras and dense/sparse traffic density button", async () => {
    render(<Analytics />);
    const streetCameras = screen.getByText(/Street Cameras/i);
    expect(streetCameras).toBeInTheDocument();
    const denseTrafficButton = screen.getByText(/Dense:/i);
    expect(denseTrafficButton).toBeInTheDocument();
    const sparseTrafficButton = screen.getByText(/Sparse:/i);
    expect(sparseTrafficButton).toBeInTheDocument();
  });

  test("should render Bus Routes", async () => {
    render(<Analytics />);
    const busRoutes = screen.getByText(/Bus Routes/i);
    expect(busRoutes).toBeInTheDocument();
  });

  test("should render Dart Stations", async () => {
    render(<Analytics />);
    const dartStations = screen.getByText(/Dart Stations/i);
    expect(dartStations).toBeInTheDocument();
  });

  test("should render Trains Running - Live", async () => {
    render(<Analytics />);
    const trainsRunning = screen.getByText(/Trains Running - Live/i);
    expect(trainsRunning).toBeInTheDocument();
  });

  test("should render Luas Stations", async () => {
    render(<Analytics />);
    const luasStations = screen.getByText(/Luas Stations/i);
    expect(luasStations).toBeInTheDocument();
  });
});
