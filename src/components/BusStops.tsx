import * as React from "react";
import L from "leaflet";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";

import iconImage from '../images/pin-blue.png'
import iconRetinaImage from '../images/pin-blue-2x.png'
import markerShadow from '../images/marker-shadow.png'

let icon = L.icon({
  iconUrl: iconImage,
  iconRetinaUrl: iconRetinaImage,
  shadowUrl: markerShadow,
  iconSize: [35, 35],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41],
});

export interface IBusStopsProps {
	busStopsData: any[]
}

export default class BusStops extends React.Component<IBusStopsProps> {
  	render() {
		return (
			<div id="map">
				<MapContainer center={[53.3548, -6.2603]} zoom={15} scrollWheelZoom={true}>
				<TileLayer
					attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
					url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
				/>

				{this.props.busStopsData.length && this.props.busStopsData.map(busStop => (
				
					<Marker key={busStop['properties']['Id']} position={[Number(busStop['geometry']['coordinates'][1]), Number(busStop['geometry']['coordinates'][0])]} icon={icon}>

					<Popup position={[Number(busStop['geometry']['coordinates'][1]), Number(busStop['geometry']['coordinates'][0])]}>
						<div>
						<div><strong>Trip ID: </strong>{busStop['properties']['TripUpdate.Trip.TripId'] }</div>
						<div><strong>Route ID: </strong>{busStop['properties']['TripUpdate.Trip.RouteId'] }</div>
						<div><strong>Stop ID: </strong>{busStop['properties']['stop_id']}</div>
						<div><strong>Stop Name: </strong>{busStop['properties']['stop_name']}</div>
						<div><strong>Stop Sequence: </strong>{busStop['properties']['TripUpdate.StopTimeUpdate.StopSequence']}</div>
						<div><strong>Delay: </strong>{busStop['properties']['TripUpdate.StopTimeUpdate.Departure.Delay']}</div>
						</div>
					</Popup>

					</Marker>

				) )}
				
				</MapContainer>
			</div>
		);
  	}
}
