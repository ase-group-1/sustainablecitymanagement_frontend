import React from 'react';
import axios from 'axios';
import { MapContainer, TileLayer, GeoJSON, Marker, Popup } from 'react-leaflet';
import L from "leaflet";
import logo from './logo.svg';
import geojson from './geojson.json';
import gtfs from './gtfs.json';

import Loader from './components/Loader';
import iconImage from './images/pin-blue.png'
import iconRetinaImage from './images/pin-blue-2x.png'
import iconRedImage from './images/pin-red.png'
import iconRedRetinaImage from './images/pin-red-2x.png'
import iconGreenImage from './images/pin-green.png'
import iconGreenRetinaImage from './images/pin-green-2x.png'
import markerShadow from './images/marker-shadow.png'
import { FeatureCollection, GeoJsonObject } from 'geojson';

// https://data.smartdublin.ie/dublinbikes-api/last_snapshot/

let icon = L.icon({
  iconUrl: iconImage,
  iconRetinaUrl: iconRetinaImage,
  shadowUrl: markerShadow,
  iconSize: [35, 35],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
})
let iconRed = L.icon({
  iconUrl: iconRedImage,
  iconRetinaUrl: iconRedRetinaImage,
  shadowUrl: markerShadow,
  iconSize: [35, 35],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
})
let iconGreen = L.icon({
  iconUrl: iconGreenImage,
  iconRetinaUrl: iconGreenRetinaImage,
  shadowUrl: markerShadow,
  iconSize: [35, 35],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
})

const views = [
  'Bikes',
  'Buses',
  'Bus Stops',
]

const dataOne: GeoJSON.Feature = {
  type: "Feature",
  properties: {
    agency_name: "Dublin Bus",
    route_id: "60-53-b12-1",
    agency_id: "978",
    route_short_name: "53",
    route_type: 3,
  },
  geometry: {
    type: "LineString",
    coordinates: [
      [-6.21697, 53.352],
      [-6.22103, 53.35215],
      [-6.22285, 53.3523],
      [-6.22284, 53.35241],
      [-6.22482, 53.35243],
      [-6.22733, 53.35387],
      [-6.22922, 53.355],
      [-6.22991, 53.3544],
      [-6.23216, 53.35257],
      [-6.23236, 53.3526],
      [-6.23249, 53.35256],
      [-6.23336, 53.35192],
      [-6.23365, 53.35176],
      [-6.23394, 53.35169],
      [-6.23445, 53.3517],
      [-6.23535, 53.35178],
      [-6.23583, 53.35186],
      [-6.23605, 53.35193],
      [-6.23784, 53.35271],
      [-6.23581, 53.35437],
      [-6.23688, 53.35482],
      [-6.2371, 53.35487],
      [-6.23734, 53.35486],
      [-6.23742, 53.35483],
      [-6.2376, 53.3547],
      [-6.23778, 53.35451],
      [-6.23885, 53.355],
      [-6.23877, 53.35547],
      [-6.23866, 53.35579],
      [-6.23843, 53.35624],
      [-6.23801, 53.35678],
      [-6.23763, 53.35722],
      [-6.23725, 53.35751],
      [-6.23534, 53.35852],
      [-6.23853, 53.36036],
      [-6.23893, 53.36039],
      [-6.23929, 53.36034],
      [-6.23972, 53.35992],
      [-6.24115, 53.35861],
      [-6.24211, 53.35795],
      [-6.24483, 53.35616],
      [-6.24619, 53.35514],
      [-6.24668, 53.35473],
      [-6.24735, 53.35407],
      [-6.24964, 53.352],
      [-6.2499, 53.35166],
      [-6.25, 53.35146],
      [-6.25102, 53.35127],
      [-6.2535, 53.35085],
    ],
  },
};

const dataTwo: GeoJSON.Feature = {
  type: "Feature",
  properties: {
    agency_name: "Dublin Bus",
    route_id: "60-52-b12-1",
    agency_id: "978",
    route_short_name: "52",
    route_type: 3,
  },
  geometry: {
    type: "LineString",
    coordinates: [
      [-6.5188, 53.37249],
      [-6.51877, 53.37254],
      [-6.51861, 53.37261],
      [-6.51843, 53.3726],
      [-6.51828, 53.37252],
      [-6.51827, 53.37242],
      [-6.51831, 53.37237],
      [-6.51844, 53.3723],
      [-6.51899, 53.3718],
      [-6.52176, 53.36862],
      [-6.52225, 53.36813],
      [-6.52278, 53.36778],
      [-6.52337, 53.36747],
      [-6.52534, 53.36673],
      [-6.52557, 53.36663],
      [-6.52592, 53.36644],
      [-6.52635, 53.36615],
      [-6.52699, 53.36559],
      [-6.52708, 53.36537],
      [-6.52703, 53.36529],
      [-6.52703, 53.36523],
    ],
  },
};

const dataThree: GeoJSON.Feature = {
  type: "Feature",
  properties: {
    agency_name: "Dublin Bus",
    route_id: "60-C2-d12-1",
    agency_id: "978",
    route_short_name: "C2",
    route_type: 3,
  },
  geometry: {
    type: "LineString",
    coordinates: [
      [-6.42151, 53.35465],
      [-6.42174, 53.35501],
      [-6.42214, 53.35557],
      [-6.4226, 53.35603],
      [-6.42507, 53.35792],
      [-6.42546, 53.35835],
      [-6.42561, 53.35857],
      [-6.4257, 53.35879],
      [-6.42577, 53.35949],
      [-6.42527, 53.35943],
      [-6.42314, 53.35905],
      [-6.42298, 53.359],
      [-6.42284, 53.35889],
      [-6.42275, 53.35891],
      [-6.42263, 53.35889],
      [-6.42259, 53.35883],
      [-6.42237, 53.35882],
      [-6.42132, 53.35897],
      [-6.4208, 53.359],
      [-6.42054, 53.35895],
      [-6.41914, 53.35906],
      [-6.41802, 53.35907],
      [-6.4151, 53.35905],
      [-6.41368, 53.35901],
      [-6.41183, 53.35886],
      [-6.41091, 53.35875],
      [-6.40938, 53.3585],
      [-6.40831, 53.35828],
      [-6.40611, 53.35776],
      [-6.40539, 53.35773],
      [-6.40489, 53.35766],
      [-6.40281, 53.35747],
      [-6.40208, 53.35742],
      [-6.40196, 53.35743],
      [-6.40171, 53.35749],
      [-6.40147, 53.35749],
      [-6.40134, 53.3574],
      [-6.40124, 53.35717],
      [-6.40114, 53.35707],
      [-6.40097, 53.35694],
      [-6.40047, 53.35665],
      [-6.40014, 53.3565],
      [-6.39945, 53.35623],
      [-6.39911, 53.35607],
      [-6.3985, 53.35596],
      [-6.39712, 53.35579],
      [-6.39651, 53.35575],
      [-6.39532, 53.35572],
      [-6.3947, 53.35574],
      [-6.39357, 53.35582],
      [-6.39247, 53.35593],
      [-6.39152, 53.35606],
      [-6.39135, 53.35612],
      [-6.39067, 53.35624],
      [-6.38844, 53.3565],
      [-6.38778, 53.3566],
      [-6.38687, 53.35679],
      [-6.38602, 53.35704],
      [-6.38521, 53.35721],
      [-6.38484, 53.35726],
      [-6.38392, 53.3573],
      [-6.38276, 53.35724],
      [-6.38139, 53.35705],
      [-6.37825, 53.35653],
      [-6.37554, 53.3559],
      [-6.37133, 53.35486],
      [-6.36706, 53.35384],
      [-6.36564, 53.35358],
      [-6.36485, 53.35347],
      [-6.36304, 53.3533],
      [-6.36122, 53.35321],
      [-6.36064, 53.35316],
      [-6.3597, 53.35297],
      [-6.35923, 53.35281],
      [-6.35871, 53.35256],
      [-6.35826, 53.35225],
      [-6.35708, 53.35123],
      [-6.35662, 53.35094],
      [-6.35634, 53.35081],
      [-6.35583, 53.35061],
      [-6.3551, 53.35041],
      [-6.3545, 53.35031],
      [-6.35311, 53.35014],
      [-6.35183, 53.34995],
      [-6.35113, 53.34977],
      [-6.35065, 53.3496],
      [-6.35016, 53.34939],
      [-6.34969, 53.34914],
      [-6.34923, 53.34881],
      [-6.34883, 53.34845],
      [-6.34716, 53.3464],
      [-6.34682, 53.34605],
      [-6.34642, 53.3457],
      [-6.34601, 53.34545],
      [-6.34548, 53.34519],
      [-6.34457, 53.34491],
      [-6.34417, 53.34482],
      [-6.34357, 53.34474],
      [-6.34036, 53.34456],
      [-6.33955, 53.34456],
      [-6.33901, 53.34458],
      [-6.33768, 53.3447],
      [-6.33611, 53.34498],
      [-6.33486, 53.34516],
      [-6.33441, 53.3452],
      [-6.33374, 53.34522],
      [-6.33308, 53.34522],
      [-6.33238, 53.34518],
      [-6.33109, 53.345],
      [-6.3267, 53.34412],
      [-6.32598, 53.344],
      [-6.32175, 53.34316],
      [-6.32079, 53.34301],
      [-6.32005, 53.34294],
      [-6.31918, 53.34291],
      [-6.3182, 53.34292],
      [-6.31135, 53.34336],
      [-6.30951, 53.34354],
      [-6.30841, 53.34369],
      [-6.30756, 53.34387],
      [-6.30712, 53.34393],
      [-6.30631, 53.34387],
      [-6.30581, 53.34399],
      [-6.30348, 53.34486],
      [-6.30327, 53.34492],
      [-6.30162, 53.3452],
      [-6.29937, 53.34555],
      [-6.29754, 53.34575],
      [-6.29483, 53.34596],
      [-6.29334, 53.34602],
      [-6.29227, 53.34616],
      [-6.29175, 53.34625],
      [-6.29156, 53.34633],
      [-6.29149, 53.34637],
      [-6.2912, 53.34686],
      [-6.29071, 53.34749],
      [-6.2897, 53.34726],
      [-6.28898, 53.34716],
      [-6.28856, 53.34713],
      [-6.2834, 53.34698],
      [-6.28088, 53.34671],
      [-6.28034, 53.34664],
      [-6.27876, 53.34631],
      [-6.27628, 53.34586],
      [-6.27592, 53.34583],
      [-6.27495, 53.3457],
      [-6.2732, 53.34556],
      [-6.27241, 53.34554],
      [-6.27122, 53.34562],
      [-6.26847, 53.34599],
      [-6.26434, 53.34636],
      [-6.26161, 53.34696],
      [-6.26002, 53.34733],
      [-6.25912, 53.34758],
      [-6.25873, 53.34696],
      [-6.25858, 53.34679],
      [-6.25831, 53.34655],
      [-6.25742, 53.34593],
      [-6.25697, 53.34591],
      [-6.25602, 53.34591],
      [-6.24935, 53.34562],
      [-6.24537, 53.34333],
      [-6.24458, 53.34317],
      [-6.24243, 53.34291],
      [-6.24046, 53.34269],
      [-6.23882, 53.34244],
      [-6.23647, 53.34217],
      [-6.23122, 53.34151],
    ],
  },
};

const dataFour: GeoJSON.Feature = {
  type: "Feature",
  properties: {
    agency_name: "Dublin Bus",
    route_id: "60-145-d12-1",
    agency_id: "978",
    route_short_name: "145",
    route_type: 3,
  },
  geometry: {
    type: "LineString",
    coordinates: [
      [-6.29207, 53.34664],
      [-6.29192, 53.34632],
      [-6.29181, 53.34624],
      [-6.29156, 53.34633],
      [-6.29149, 53.34637],
      [-6.2912, 53.34686],
      [-6.29071, 53.34749],
      [-6.2897, 53.34726],
      [-6.28898, 53.34716],
      [-6.28856, 53.34713],
      [-6.2834, 53.34698],
      [-6.28088, 53.34671],
      [-6.28034, 53.34664],
      [-6.27843, 53.34626],
      [-6.27628, 53.34586],
      [-6.27592, 53.34583],
      [-6.27495, 53.3457],
      [-6.27281, 53.34555],
      [-6.27241, 53.34554],
      [-6.27122, 53.34562],
      [-6.26862, 53.34597],
      [-6.26731, 53.3461],
      [-6.26628, 53.34617],
      [-6.26434, 53.34636],
      [-6.26002, 53.34733],
      [-6.25912, 53.34758],
      [-6.25868, 53.3469],
      [-6.25858, 53.34679],
      [-6.25826, 53.34652],
      [-6.25742, 53.34593],
      [-6.25734, 53.34585],
      [-6.25735, 53.34573],
      [-6.2574, 53.34569],
      [-6.25839, 53.34535],
      [-6.25897, 53.34505],
      [-6.25919, 53.34487],
      [-6.25953, 53.34446],
      [-6.25923, 53.34408],
      [-6.25921, 53.34399],
      [-6.2592, 53.34372],
      [-6.25927, 53.34352],
      [-6.25928, 53.3434],
      [-6.25916, 53.34326],
      [-6.25904, 53.34321],
      [-6.25506, 53.3423],
      [-6.25457, 53.34217],
    ],
  },
};

const geoJsonObj: GeoJSON.FeatureCollection = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      properties: {
        agency_name: "Dublin Bus",
        route_id: "60-53-b12-1",
        agency_id: "978",
        route_short_name: "53",
        route_type: 3,
      },
      geometry: {
        type: "LineString",
        coordinates: [
          [-6.19562, 53.34574],
          [-6.19572, 53.34574],
          [-6.19584, 53.3457],
          [-6.19602, 53.3457],
          [-6.1961, 53.34575],
          [-6.19609, 53.34585],
          [-6.19467, 53.34585],
          [-6.19417, 53.34584],
          [-6.19391, 53.34588],
          [-6.1938, 53.34609],
          [-6.19378, 53.34691],
          [-6.19383, 53.34713],
          [-6.19426, 53.34762],
          [-6.19446, 53.34782],
          [-6.19457, 53.34785],
          [-6.19459, 53.3479],
          [-6.19443, 53.34818],
          [-6.19446, 53.35009],
          [-6.19442, 53.35015],
          [-6.19454, 53.35019],
          [-6.19483, 53.35022],
          [-6.1961, 53.35023],
          [-6.19619, 53.35026],
          [-6.19684, 53.35022],
          [-6.19867, 53.35023],
          [-6.20274, 53.3502],
          [-6.20871, 53.35044],
          [-6.20859, 53.35167],
          [-6.21039, 53.35173],
          [-6.21171, 53.3518],
          [-6.21217, 53.35181],
          [-6.21344, 53.35187],
          [-6.21365, 53.35191],
          [-6.21433, 53.35189],
          [-6.21697, 53.352],
          [-6.2169, 53.35295],
        ],
      },
    },
    {
      type: "Feature",
      properties: {
        agency_name: "Dublin Bus",
        route_id: "60-52-b12-1",
        agency_id: "978",
        route_short_name: "52",
        route_type: 3,
      },
      geometry: {
        type: "LineString",
        coordinates: [
          [-6.5188, 53.37249],
          [-6.51877, 53.37254],
          [-6.51861, 53.37261],
          [-6.51843, 53.3726],
          [-6.51828, 53.37252],
          [-6.51827, 53.37242],
          [-6.51831, 53.37237],
          [-6.51844, 53.3723],
          [-6.51899, 53.3718],
          [-6.52176, 53.36862],
          [-6.52225, 53.36813],
          [-6.52278, 53.36778],
          [-6.52337, 53.36747],
          [-6.52534, 53.36673],
          [-6.52557, 53.36663],
          [-6.52592, 53.36644],
          [-6.52635, 53.36615],
          [-6.52699, 53.36559],
          [-6.52708, 53.36537],
          [-6.52703, 53.36529],
          [-6.52703, 53.36523],
        ],
      },
    },
    {
      type: "Feature",
      properties: {
        agency_name: "Dublin Bus",
        route_id: "60-C2-d12-1",
        agency_id: "978",
        route_short_name: "C2",
        route_type: 3,
      },
      geometry: {
        type: "LineString",
        coordinates: [
          [-6.42151, 53.35465],
          [-6.42174, 53.35501],
          [-6.42214, 53.35557],
          [-6.4226, 53.35603],
          [-6.42507, 53.35792],
          [-6.42546, 53.35835],
          [-6.42561, 53.35857],
          [-6.4257, 53.35879],
          [-6.42577, 53.35949],
          [-6.42527, 53.35943],
          [-6.42314, 53.35905],
          [-6.42298, 53.359],
          [-6.42284, 53.35889],
          [-6.42275, 53.35891],
          [-6.42263, 53.35889],
          [-6.42259, 53.35883],
          [-6.42237, 53.35882],
          [-6.42132, 53.35897],
          [-6.4208, 53.359],
          [-6.42054, 53.35895],
          [-6.41914, 53.35906],
          [-6.41802, 53.35907],
          [-6.4151, 53.35905],
          [-6.41368, 53.35901],
          [-6.41183, 53.35886],
          [-6.41091, 53.35875],
          [-6.40938, 53.3585],
          [-6.40831, 53.35828],
          [-6.40611, 53.35776],
          [-6.40539, 53.35773],
          [-6.40489, 53.35766],
          [-6.40281, 53.35747],
          [-6.40208, 53.35742],
          [-6.40196, 53.35743],
          [-6.40171, 53.35749],
          [-6.40147, 53.35749],
          [-6.40134, 53.3574],
          [-6.40124, 53.35717],
          [-6.40114, 53.35707],
          [-6.40097, 53.35694],
          [-6.40047, 53.35665],
          [-6.40014, 53.3565],
          [-6.39945, 53.35623],
          [-6.39911, 53.35607],
          [-6.3985, 53.35596],
          [-6.39712, 53.35579],
          [-6.39651, 53.35575],
          [-6.39532, 53.35572],
          [-6.3947, 53.35574],
          [-6.39357, 53.35582],
          [-6.39247, 53.35593],
          [-6.39152, 53.35606],
          [-6.39135, 53.35612],
          [-6.39067, 53.35624],
          [-6.38844, 53.3565],
          [-6.38778, 53.3566],
          [-6.38687, 53.35679],
          [-6.38602, 53.35704],
          [-6.38521, 53.35721],
          [-6.38484, 53.35726],
          [-6.38392, 53.3573],
          [-6.38276, 53.35724],
          [-6.38139, 53.35705],
          [-6.37825, 53.35653],
          [-6.37554, 53.3559],
          [-6.37133, 53.35486],
          [-6.36706, 53.35384],
          [-6.36564, 53.35358],
          [-6.36485, 53.35347],
          [-6.36304, 53.3533],
          [-6.36122, 53.35321],
          [-6.36064, 53.35316],
          [-6.3597, 53.35297],
          [-6.35923, 53.35281],
          [-6.35871, 53.35256],
          [-6.35826, 53.35225],
          [-6.35708, 53.35123],
          [-6.35662, 53.35094],
          [-6.35634, 53.35081],
          [-6.35583, 53.35061],
          [-6.3551, 53.35041],
          [-6.3545, 53.35031],
          [-6.35311, 53.35014],
          [-6.35183, 53.34995],
          [-6.35113, 53.34977],
          [-6.35065, 53.3496],
          [-6.35016, 53.34939],
          [-6.34969, 53.34914],
          [-6.34923, 53.34881],
          [-6.34883, 53.34845],
          [-6.34716, 53.3464],
          [-6.34682, 53.34605],
          [-6.34642, 53.3457],
          [-6.34601, 53.34545],
          [-6.34548, 53.34519],
          [-6.34457, 53.34491],
          [-6.34417, 53.34482],
          [-6.34357, 53.34474],
          [-6.34036, 53.34456],
          [-6.33955, 53.34456],
          [-6.33901, 53.34458],
          [-6.33768, 53.3447],
          [-6.33611, 53.34498],
          [-6.33486, 53.34516],
          [-6.33441, 53.3452],
          [-6.33374, 53.34522],
          [-6.33308, 53.34522],
          [-6.33238, 53.34518],
          [-6.33109, 53.345],
          [-6.3267, 53.34412],
          [-6.32598, 53.344],
          [-6.32175, 53.34316],
          [-6.32079, 53.34301],
          [-6.32005, 53.34294],
          [-6.31918, 53.34291],
          [-6.3182, 53.34292],
          [-6.31135, 53.34336],
          [-6.30951, 53.34354],
          [-6.30841, 53.34369],
          [-6.30756, 53.34387],
          [-6.30712, 53.34393],
          [-6.30631, 53.34387],
          [-6.30581, 53.34399],
          [-6.30348, 53.34486],
          [-6.30327, 53.34492],
          [-6.30162, 53.3452],
          [-6.29937, 53.34555],
          [-6.29754, 53.34575],
          [-6.29483, 53.34596],
          [-6.29334, 53.34602],
          [-6.29227, 53.34616],
          [-6.29175, 53.34625],
          [-6.29156, 53.34633],
          [-6.29149, 53.34637],
          [-6.2912, 53.34686],
          [-6.29071, 53.34749],
          [-6.2897, 53.34726],
          [-6.28898, 53.34716],
          [-6.28856, 53.34713],
          [-6.2834, 53.34698],
          [-6.28088, 53.34671],
          [-6.28034, 53.34664],
          [-6.27876, 53.34631],
          [-6.27628, 53.34586],
          [-6.27592, 53.34583],
          [-6.27495, 53.3457],
          [-6.2732, 53.34556],
          [-6.27241, 53.34554],
          [-6.27122, 53.34562],
          [-6.26847, 53.34599],
          [-6.26434, 53.34636],
          [-6.26161, 53.34696],
          [-6.26002, 53.34733],
          [-6.25912, 53.34758],
          [-6.25873, 53.34696],
          [-6.25858, 53.34679],
          [-6.25831, 53.34655],
          [-6.25742, 53.34593],
          [-6.25697, 53.34591],
          [-6.25602, 53.34591],
          [-6.24935, 53.34562],
          [-6.24537, 53.34333],
          [-6.24458, 53.34317],
          [-6.24243, 53.34291],
          [-6.24046, 53.34269],
          [-6.23882, 53.34244],
          [-6.23647, 53.34217],
          [-6.23122, 53.34151],
        ],
      },
    },
    {
      type: "Feature",
      properties: {
        agency_name: "Dublin Bus",
        route_id: "60-145-d12-1",
        agency_id: "978",
        route_short_name: "145",
        route_type: 3,
      },
      geometry: {
        type: "LineString",
        coordinates: [
          [-6.29207, 53.34664],
          [-6.29192, 53.34632],
          [-6.29181, 53.34624],
          [-6.29156, 53.34633],
          [-6.29149, 53.34637],
          [-6.2912, 53.34686],
          [-6.29071, 53.34749],
          [-6.2897, 53.34726],
          [-6.28898, 53.34716],
          [-6.28856, 53.34713],
          [-6.2834, 53.34698],
          [-6.28088, 53.34671],
          [-6.28034, 53.34664],
          [-6.27843, 53.34626],
          [-6.27628, 53.34586],
          [-6.27592, 53.34583],
          [-6.27495, 53.3457],
          [-6.27281, 53.34555],
          [-6.27241, 53.34554],
          [-6.27122, 53.34562],
          [-6.26862, 53.34597],
          [-6.26731, 53.3461],
          [-6.26628, 53.34617],
          [-6.26434, 53.34636],
          [-6.26002, 53.34733],
          [-6.25912, 53.34758],
          [-6.25868, 53.3469],
          [-6.25858, 53.34679],
          [-6.25826, 53.34652],
          [-6.25742, 53.34593],
          [-6.25734, 53.34585],
          [-6.25735, 53.34573],
          [-6.2574, 53.34569],
          [-6.25839, 53.34535],
          [-6.25897, 53.34505],
          [-6.25919, 53.34487],
          [-6.25953, 53.34446],
          [-6.25923, 53.34408],
          [-6.25921, 53.34399],
          [-6.2592, 53.34372],
          [-6.25927, 53.34352],
          [-6.25928, 53.3434],
          [-6.25916, 53.34326],
          [-6.25904, 53.34321],
          [-6.25506, 53.3423],
          [-6.25457, 53.34217],
        ],
      },
    },
  ],
};

type Geo = {
  type: string,
  features: any
};

// type GtfsFeatures = {
//   Id: string,
//   IsDeleted: boolean,
//   Timestamp: string,
//   stop_id: string,
//   stop_name: string
// }

type Gtfs = {
  type: string,
  crs: any,
  features: any,
};

const geoJson = geojson as Geo;
const gtfsObj = gtfs as Gtfs;

console.log(geoJson.features[0].properties.route_short_name);

interface IProps {
}

interface IState {
  view: string,
  defaultCenter: any,
  defaultZoom: number,
  busNumbers: string[],
  bikesData: string[],
  busStopsData: string[],
  busRoute: string,
  projectedBikes: number,
  routes: GeoJSON.FeatureCollection
}

class App extends React.Component<IProps, IState> {

  constructor(props: IProps) {
    super(props);

    this.state = {
      view: 'Bikes',
      defaultCenter: { lat: 53.3548, lng: -6.2603 },
      defaultZoom: 13,
      busNumbers: ['1','11','116', '118','120','122','123'],
      bikesData: [],
      busStopsData: [],
      busRoute: '53',
      // busData: dataOne,
      projectedBikes: 0,
      routes: {
        type: "FeatureCollection",
        features: [],
      }
    };
  }
  componentDidMount = () => {
    console.log("app mounted")
    const busStopsData = gtfsObj.features;
    axios.get("http://127.0.0.1:5000/bikesdata")
      .then(response => {
        const bikesData = response.data;
        this.setState({
          bikesData,
          busStopsData
        });
      })
      // axios.get("http://127.0.0.1:5000/mldata")
      // .then(response => {
      //   const projectedBikes = response.data;
      //   this.setState({
      //     projectedBikes
      //   });
      // })
  }
  loader = () => {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Loading...
          </p>
        </header>
      </div>
    )
  }
  filterRoutes = async ( route: string ) => {
    console.log('filters...')
    var routes: GeoJSON.FeatureCollection = await geoJson.features.filter( function( item: any ) {
      if ( route === item.properties.route_short_name ) {
        return item as GeoJSON.Feature;
      }
      return false;
    });
    this.setState({
      routes: routes
    })
  }
  getBusRouteOptions = () => {
    const unique = Array.from(new Set(geoJson.features.map((item: any) => {
      if ( item.properties.route_short_name ) {
        return item.properties.route_short_name
      } else if ( item.properties.routes ) {
        return item.properties.routes[0].route_short_name
      } else {
        return null
      }
    })));
    const options = unique.map( (route: any ) => {
      return <option key={route} value={route}>{route}</option>
    } )
    return options
  }
  selectBusRoutes = () => {
    return <>
      <label htmlFor="country" className="block text-sm font-medium text-gray-700">
        Select a route
      </label>
      <select
        id="route"
        name="route"
        className="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
        onChange={ e => this.changeBusRoute(e) }
      >
        <option value=""></option>
        {this.getBusRouteOptions()}
      </select>
    </>
  }
  changeBusRoute = ( event: React.ChangeEvent<HTMLSelectElement> ) => {
    this.setState({
      busRoute: event.target.value
    })
    this.filterRoutes( event.target.value )
  }
  bikesView = () => {
    return (
      <div id="map">
        <MapContainer center={[53.3548, -6.2603]} zoom={15} scrollWheelZoom={true}>
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />

          {this.state.bikesData.length && this.state.bikesData.map(bikeStation => (
          
            <Marker key={bikeStation['id' as any]} position={[Number(bikeStation['lat' as any]), Number(bikeStation['longi' as any])]} icon={ ( 25 >= ( 100*Number(bikeStation['avl_bks' as any]) ) / Number(bikeStation['bks_std' as any]) ) ? iconRed : ( 75 < ( 100*Number(bikeStation['avl_bks' as any]) ) / Number(bikeStation['bks_std' as any]) ) ? iconGreen : icon}>

              <Popup position={[Number(bikeStation['lat' as any]), Number(bikeStation['longi' as any])]}>
                <div>
                  <h2>{"Name: " + bikeStation['addr_ln' as any]}</h2>
                  <h2>{"Status: " + bikeStation['sta' as any] }</h2>
                  <h2>{"Available Bikes: " + bikeStation['avl_bks' as any]}</h2>
                  <h2>{"Bike Stand Capacity: " + bikeStation['bks_std' as any]}</h2>
                  <h2>{"Available Bikes after 1 Hr: " + this.state.projectedBikes}</h2>
                </div>
              </Popup>

            </Marker>

          ) )}
        
        </MapContainer>
      </div>
    )
  }
  busView = () => {
    return (
      <MapContainer center={this.state.defaultCenter} zoom={this.state.defaultZoom}>
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" attribution="&copy; <a href=&quot;https:www.openstreetmap.org/copyright&quot;>OpenStreetMap</a> contributors" />
        <GeoJSON key={this.state.busRoute} data={this.state.routes} />
      </MapContainer>
    )
  }
  busStopsView = () => {
    return (
      <div id="map">
        <MapContainer center={[53.3548, -6.2603]} zoom={15} scrollWheelZoom={true}>
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />

          {this.state.busStopsData.length && this.state.busStopsData.map(busStop => (
          
            <Marker key={busStop['properties' as any]['Id' as any]} position={[Number(busStop['geometry' as any]['coordinates' as any][1]), Number(busStop['geometry' as any]['coordinates' as any][0])]} icon={icon}>

              <Popup position={[Number(busStop['geometry' as any]['coordinates' as any][1]), Number(busStop['geometry' as any]['coordinates' as any][0])]}>
                <div>
                  <div><strong>Trip ID: </strong>{busStop['properties' as any]['TripUpdate.Trip.TripId' as any] }</div>
                  <div><strong>Route ID: </strong>{busStop['properties' as any]['TripUpdate.Trip.RouteId' as any] }</div>
                  <div><strong>Stop ID: </strong>{busStop['properties' as any]['stop_id' as any]}</div>
                  <div><strong>Stop Name: </strong>{busStop['properties' as any]['stop_name' as any]}</div>
                  <div><strong>Stop Sequence: </strong>{busStop['properties' as any]['TripUpdate.StopTimeUpdate.StopSequence' as any]}</div>
                  <div><strong>Delay: </strong>{busStop['properties' as any]['TripUpdate.StopTimeUpdate.Departure.Delay' as any]}</div>
                </div>
              </Popup>

            </Marker>

          ) )}
        
        </MapContainer>
      </div>
    )
  }
  changeView = ( event: React.ChangeEvent<HTMLSelectElement> ) => {
    console.log( event.target.value )
    this.setState({
      view: event.target.value
    })
  }
  render = () => {
    let view;
    if ( this.state.bikesData.length && 'Bikes' === this.state.view ) {
      view = this.bikesView()
    } else if ( 'Buses' === this.state.view ) {
      view = this.busView()
    } else if ( 'Bus Stops' === this.state.view ) {
      view = this.busStopsView()
    } else {
      view = Loader()
    }
    return (
    <div className="grid lg:grid-cols-4 gap-4 md:grid-cols-12 App">
      <div className='col-span-1'>
        <div className="justify-center">
          <div className="mb-3">
            <label htmlFor="country" className="block text-sm font-medium text-gray-700">
              Select a view
            </label>
            <select
              id="country"
              name="country"
              autoComplete="country-name"
              className="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
              onChange={ e => this.changeView(e) }
            >
              {views.length && views.map( view => <option key={view} value={view}>{view}</option> )}
            </select>
          </div>
          <div className="mb-3">
            {'Buses' === this.state.view && this.selectBusRoutes()}
          </div>
        </div>
      </div>
      <div className='col-span-3'>{view}</div>
    </div>
    )
  }
}

export default App;
