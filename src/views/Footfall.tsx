import React, { useEffect } from "react";
import {
    LayerGroup,
    CircleMarker,
    Popup,
    MapContainer,
    TileLayer,
} from "react-leaflet";
import axios from "axios";
import TrafficDensity from "../components/TrafficDensity";

export default function Footfall() {
    const [ loading, setLoading ] = React.useState<boolean>(true)
    const [footfallData, setFootfallData] = React.useState<{lat:string, long:string, footfall:number}[]>([]);

    useEffect(() => {
        async function fetchTrafficData(apiUrl: string) {
            await axios
                .get(apiUrl)
                .then((response) => {
                    console.log(
                        `Footfall API Request Status: ${response.status}`
                    );
                    if (200 === response.status) {
                        setFootfallData(response.data)
                        setLoading(false)
                    }
                })
                .catch(function (error) {
                    console.log(error.toJSON());
                });
        }
        fetchTrafficData( process.env.REACT_APP_FOOTFALL_DATA_API as string );
    }, []);

    return (
        <div id="map">
            { loading ? <div className="loader"></div> : '' }
            <div className={loading ? "footfall-loading" : ""}>
                <MapContainer
                    center={[53.3548, -6.2603]}
                    zoom={11}
                    scrollWheelZoom={true}
                >
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <TrafficDensity />
                    <LayerGroup>
                        {footfallData.length &&
                            footfallData.map((data: {lat:string, long:string, footfall:number}, index: number) => {
                                console.log(data)
                                return (
                                    <CircleMarker
                                        key={index}
                                        center={[
                                            parseFloat(data.lat),
                                            parseFloat(data.long),
                                        ]}
                                        pathOptions={{ fillColor: "red" }}
                                        radius={20}
                                        stroke={false}
                                    >
                                        <Popup>{`Footfall: ${data.footfall}`}</Popup>
                                    </CircleMarker>
                                );
                            })}
                    </LayerGroup>
                </MapContainer>
            </div>
        </div>
    );
}
