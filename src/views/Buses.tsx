import React, { useEffect } from "react"
import Select from 'react-select'
import geojson from "../data/shapes_gdf.json"
import { LatLngExpression } from "leaflet"
import dropdownOptions from "../data/dropdown_options.json"
import aTI from "../data/active_trip_ids.json"
import routeShapes from "../data/shapes_stops_info.json"

// import Loader from "./components/Loader";
import BusRoutes from "../components/BusRoutes"
import axios from "axios"

type Geo = {
    type: string;
    features: any;
};

const geoJson = geojson as Geo;

const polygons:LatLngExpression[][] = [
    [
      [51.51, -0.12],
      [51.51, -0.13],
      [51.53, -0.13],
    ],
    [
      [51.51, -0.05],
      [51.51, -0.07],
      [51.53, -0.07],
    ],
]

export default function Buses() {

    const [ mapOptions, setMapOptions ] = React.useState( { defaultCenter: { lat: 53.3548, lng: -6.2603 }, defaultZoom: 13 })
    const [ busRoute, setBusRoute ]     = React.useState("")
    const [ routes, setRoutes ]         = React.useState({ type: "FeatureCollection", features: [] })
    const [ route, setRoute ]           = React.useState('')
    const [ agency, setAgency ]           = React.useState('')
    const [ direction, setDirection ]           = React.useState('')
    const [ activeTripIds, setActiveTripIds ] = React.useState({})
    const [ activeTripId, setActiveTripId ] = React.useState('')
    const [ shapes, setShapes ] = React.useState({ type: "FeatureCollection", features: [] })
    const [ stops, setStops ] = React.useState({})

    useEffect(() => {
        setActiveTripIds(aTI)
        setShapes(JSON.parse(routeShapes.shape))
        setStops(JSON.parse(routeShapes.stops_info))
    },[])

    useEffect(() => {
        const lat = routes.features[0] ? routes.features[0]['geometry']['coordinates'][0][1] : 53.3548
        const lng = routes.features[0] ? routes.features[0]['geometry']['coordinates'][0][0] : -6.2603

        setMapOptions({
            defaultCenter: {
                lat: lat,
                lng: lng
            },
            defaultZoom: 10
        })
    }, [routes]);

    function filterRoutes(route: string) {
        var routesData = geoJson.features.filter(function (item: any) {
            if (route === item.properties.route_short_name) {
                return item;
            }
            return false;
        });
        return routesData;
    };

    function handleBusRoute(event: any) {
        setBusRoute(event.value)

        const filteredRoutes = filterRoutes( event.value )
        setRoutes( { type: "FeatureCollection", features: filteredRoutes } )
    }

    function busRouteOptions() {
        const unique = Array.from(
            new Set(
                geoJson.features.map((item: any) => {
                    if (item.properties.route_short_name) {
                        return item.properties.route_short_name;
                    } else if (item.properties.routes) {
                        return item.properties.routes[0].route_short_name;
                    } else {
                        return null;
                    }
                })
            )
        );
        const options = unique.map((route: any) => {
            return { value: route, label: route };
                // <option key={route} value={route}>
                //     {route}
                // </option>
            // ;
        });
        return options;
    }

    const handleRoute = (e: any) => {
        setRoute(e.value)
        if (agency && '' !== direction) {
            console.log(agency, e.value, direction)
            getBusTripIds(agency, e.value, direction).then((data: any) => {
                console.log(data)
            })
        }
    }

    const handleAgency = (e: any) => {
        setAgency(e.value)
        if (route && '' !== direction) {
            console.log(e.value, route, direction)
            getBusTripIds(e.value, route, direction).then((data: any) => {
                console.log(data)
            })
        }
    }

    const handleDirection = (e: any) => {
        setDirection(e.value)
        if (route && agency) {
            console.log(agency, route, e.value)
            getBusTripIds(agency,route,e.value).then((data: any) => {
                console.log(data)
            })
        }
    }

    const getRouteNames = () => {
        const names = dropdownOptions.map((item: any) => {
            return {
                label: item.route_s_nm,
                value: item.route_id
            }
        })
        return names
    }

    const getBusTripIds = async (agency_id: string, route_id: string, dir_id: string) => {
        try {
            // const response = await axios.get(`https://flask-python-webapp-quickstart-ase.azurewebsites.net/bustripids?agency_id=${agency_id}&route_id=${route_id}&d_id=${dir_id}`);
            // return response.data;
            return activeTripIds
        } catch (error) {
            console.error('Error bus trips:', error);
            throw error;
        }
    }

    const getActiveTripIdOptions = () => {
        const ati: any = aTI
        console.log(ati)
        const names = ati.trip_id.map((item: any) => {
            return {
                label: item,
                value: item
            }
        })
        return names
    }

    const setActiveTripIdOption = (e: any) => {
        console.log(e.value)
        setActiveTripId(e.value)
    }

    return (
        <>
            <Select className="react-select-container" options={getRouteNames()} placeholder="Select a route" onChange={handleRoute} menuPortalTarget={document.body} styles={{
                control: base => ({ ...base, marginBottom: 20 }),
                menuPortal: base => ({ ...base, zIndex: 9999 }),
                input: base => ({ ...base, boxShadow: 'none' }),
            }} />
            <Select className="react-select-container" options={[{
                'label': 0,
                'value': 0
            },{
                'label': 1,
                'value': 1
            }]} placeholder="Select a direction" onChange={handleDirection} menuPortalTarget={document.body} styles={{
                control: base => ({ ...base, marginBottom: 20 }),
                menuPortal: base => ({ ...base, zIndex: 9999 }),
                input: base => ({ ...base, boxShadow: 'none' }),
            }} />
            <Select className="react-select-container" options={[{
                'label': 'Dublin Bus',
                'value': '7778019'
            }]} placeholder="Select an agency" onChange={handleAgency} menuPortalTarget={document.body} styles={{
                control: base => ({ ...base, marginBottom: 20 }),
                menuPortal: base => ({ ...base, zIndex: 9999 }),
                input: base => ({ ...base, boxShadow: 'none' }),
            }} />
            <Select className="react-select-container" options={getActiveTripIdOptions()} placeholder="Select trip id" onChange={setActiveTripIdOption} menuPortalTarget={document.body} styles={{
                control: base => ({ ...base, marginBottom: 20 }),
                menuPortal: base => ({ ...base, zIndex: 9999 }),
                input: base => ({ ...base, boxShadow: 'none' }),
            }} />
            {/* <Select className="react-select-container" options={busRouteOptions()} placeholder="Select a route" onChange={handleBusRoute} menuPortalTarget={document.body} styles={{
                control: base => ({ ...base, marginBottom: 20 }),
                menuPortal: base => ({ ...base, zIndex: 9999 }),
                input: base => ({ ...base, boxShadow: 'none' }),
            }} /> */}
            <BusRoutes
                defaultCenter={mapOptions.defaultCenter}
                defaultZoom={mapOptions.defaultZoom}
                busRoute={busRoute}
                routes={routes}
                polygons={polygons}
                stops={stops}
                shapes={shapes}
                route={route}
                activeTripId={activeTripId}
            />
        </>
    );
}
