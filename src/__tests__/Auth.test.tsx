import { render, screen, waitFor, fireEvent } from '@testing-library/react';
import axios, { AxiosResponse } from 'axios';
import AuthProvider, { AuthContext, useAuth } from '../Auth';

jest.mock('axios');

const mockedAxios = axios as jest.Mocked<typeof axios>;

// Test component to access the useAuth hook values
function TestComponent() {
  const { auth, setAuth, user, fetchUserDetails, login, signup, logout } = useAuth();

  return (
    <div>
      <span data-testid="auth">{auth.toString()}</span>
      <span data-testid="user">{user.toString()}</span>
      <button data-testid="login" onClick={() => login('test@example.com', 'password')}></button>
      <button data-testid="signup" onClick={() => signup('Test User', 'test@example.com', 'password', 'user')}></button>
      <button data-testid="logout" onClick={() => logout()}></button>
    </div>
  );
}

// Add this helper component to access the fetchUserDetails function
function FetchUserDetailsTestComponent() {
  const { fetchUserDetails } = useAuth();

  return (
    <div>
      <button
        data-testid="fetchUserDetails"
        onClick={async () => {
          const userDetails = await fetchUserDetails();
          console.log('Fetched user details:', userDetails);
        }}
      ></button>
    </div>
  );
}

describe('AuthProvider', () => {

  beforeEach(() => {
    localStorage.clear();
  });  

  test('initial auth and user state', () => {
    render(
      <AuthProvider>
        <TestComponent />
      </AuthProvider>
    );

    expect(screen.getByTestId('auth').textContent).toBe('false');
    expect(screen.getByTestId('user').textContent).toBe('false');
  });

  test('login', async () => {
    localStorage.setItem('scdAuth', 'false');
  
    render(
      <AuthProvider>
        <TestComponent />
      </AuthProvider>
    );
  
    mockedAxios.post.mockResolvedValueOnce({ data: {} } as AxiosResponse);
  
    fireEvent.click(screen.getByTestId('login'));
  
    await waitFor(() => {
      expect(localStorage.getItem('scdAuth')).toBe('true');
    });
  });

  test('signup', async () => {
    render(
      <AuthProvider>
        <TestComponent />
      </AuthProvider>
    );

    mockedAxios.post.mockResolvedValueOnce({ data: {} } as AxiosResponse);

    fireEvent.click(screen.getByTestId('signup'));

    await waitFor(() => expect(mockedAxios.post).toHaveBeenCalledWith('https://flask-python-webapp-quickstart-ase.azurewebsites.net/signup', {
      name: 'Test User',
      email: 'test@example.com',
      password: 'password',
      role: 'user',
    }));
  });

  test('logout', async () => {
    localStorage.setItem('scdAuth', 'true');
  
    render(
      <AuthProvider>
        <TestComponent />
      </AuthProvider>
    );
  
    fireEvent.click(screen.getByTestId('logout'));
  
    await waitFor(() => {
      expect(localStorage.getItem('scdAuth')).toBeNull();
    });
  });
  

  // Update the fetchUserDetails test case
  test('fetchUserDetails', async () => {
    render(
      <AuthProvider>
        <FetchUserDetailsTestComponent />
      </AuthProvider>
    );

    const userDetails = { name: 'Test User', email: 'test@example.com' };
    mockedAxios.get.mockResolvedValueOnce({ data: userDetails } as AxiosResponse);

    fireEvent.click(screen.getByTestId('fetchUserDetails'));

    await waitFor(() => expect(mockedAxios.get).toHaveBeenCalledWith('https://flask-python-webapp-quickstart-ase.azurewebsites.net/user'));
  });
});
